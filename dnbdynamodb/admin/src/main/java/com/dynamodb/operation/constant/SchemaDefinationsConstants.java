package com.dynamodb.operation.constant;

/**
 * class for declaring constant
 * @author IMPETUS
 */

/**
 * <p>
 * This is the class for declaring constant value which are used in application.
 * </p>
 * @author impadmin
 * @since 08-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public interface SchemaDefinationsConstants
{
  String CIC_CSV_HEADER_LINE = "DUNS_NO,DATE_CREATED,COUNTRY,RECORD_NO";
  String COLUMN_DELIMITER = ",";
  String EXPORT_FILE_NAME_EXT = ".csv";
  String CIC_DUNS_NO = "DUNS_NO";
  String CIC_DATE_CREATED = "DATE_CREATED";
  String CIC_COUNTRY = "COUNTRY";
  String CIC_RECORD_NO = "RECORD_NO";  
  
  String AUDIT_MONITOR = "DNB_CIRRUS_FEED_AUDIT";
  String COLUMN_DELIMITER_AUDIT = "\t";
  String AUDIT_HEADER_LINE = "RUN_DATE" + "\t" + "FILE_PROCESSED" + "\t" + "TOT_REC" + "\t" + "ERR_REC" + "\t" + "SUCC_REC";

}
