package com.dynamodb.operation.bean;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;


/**
 * <p>
 * Pojo class for DNB_CIRRUS_CIC_INPUT table
 * </p>
 * @author impadmin
 * @since 08-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
@DynamoDBTable(tableName = "DNB_CIRRUS_FEED_AUDIT")
public class Audit
{
  
  private String processedDate;
  private String processedFile;
  private String totalCount;
  private String errorCount;
  private String successCount;
   
  /**
   * <p>
   * See {@link #setprocessedDate(String)}
   * </p>
   * @return Returns the processedDate.
   */
  @DynamoDBHashKey(attributeName = "PROCESSED_DATE")
  public String getProcessedDate()
  {
    return processedDate;
  }
  /**
   * <p>
   * Set the value of <code>processedDate</code>.
   * </p>
   * @param processedDate The processedDate to set.
   */
  public void setProcessedDate( String processedDate )
  {
    this.processedDate = processedDate;
  }
  /**
   * <p>
   * See {@link #setprocessedFile(String)}
   * </p>
   * @return Returns the processedFile.
   */
  @DynamoDBRangeKey(attributeName = "PROCESSED_FILE")
  public String getProcessedFile()
  {
    return processedFile;
  }
  /**
   * <p>
   * Set the value of <code>processedFile</code>.
   * </p>
   * @param processedFile The processedFile to set.
   */
  public void setProcessedFile( String processedFile )
  {
    this.processedFile = processedFile;
  }
  /**
   * <p>
   * See {@link #settotalCount(String)}
   * </p>
   * @return Returns the totalCount.
   */
  @DynamoDBAttribute(attributeName = "TOTAL_COUNT")
  public String getTotalCount()
  {
    return totalCount;
  }
  /**
   * <p>
   * Set the value of <code>totalCount</code>.
   * </p>
   * @param totalCount The totalCount to set.
   */
  public void setTotalCount( String totalCount )
  {
    this.totalCount = totalCount;
  }
  /**
   * <p>
   * See {@link #seterrorCount(String)}
   * </p>
   * @return Returns the errorCount.
   */
  @DynamoDBAttribute(attributeName = "ERROR_COUNT")
  public String getErrorCount()
  {
    return errorCount;
  }
  /**
   * <p>
   * Set the value of <code>errorCount</code>.
   * </p>
   * @param errorCount The errorCount to set.
   */
  public void setErrorCount( String errorCount )
  {
    this.errorCount = errorCount;
  }
  /**
   * <p>
   * See {@link #setsuccessCount(String)}
   * </p>
   * @return Returns the successCount.
   */
  @DynamoDBAttribute(attributeName = "SUCCESS_COUNT")
  public String getSuccessCount()
  {
    return successCount;
  }
  /**
   * <p>
   * Set the value of <code>successCount</code>.
   * </p>
   * @param successCount The successCount to set.
   */
  public void setSuccessCount( String successCount )
  {
    this.successCount = successCount;
  }  
  
}
