package com.dynamodb.operation.bean;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

/**
 * <p>
 * Pojo class for DNB_CIRRUS_CIC_INPUT table
 * </p>
 * @author impadmin
 * @since 08-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
@DynamoDBTable(tableName = "DNB_CIRRUS_CIC_INPUT")
public class CIC_INPUT
{

  private String duns_no;
  private String date_created;
  private String country;
  private Integer record_no;   

  /**
   * @return
   */
  @DynamoDBHashKey(attributeName = "DATE_CREATED")
  public String getDate_created()
  {
    return date_created;
  }

  /**
   * @param date_created
   */
  public void setDate_created( String date_created )
  {
    this.date_created = date_created;
  }

  /**
   * @return the record_no
   */

  @DynamoDBRangeKey(attributeName = "RECORD_NO")
  public Integer getRecord_no()
  {
    return record_no;
  }

  /**
   * @param record_no
   *          the record_no to set
   */
  public void setRecord_no( Integer record_no )
  {
    this.record_no = record_no;
  }

  /**
   * @return
   */
  @DynamoDBAttribute(attributeName = "COUNTRY")
  public String getCountry()
  {
    return country;
  }

  /**
   * @param country
   */
  public void setCountry( String country )
  {
    this.country = country;
  }

  /**
   * @return
   */
  @DynamoDBAttribute(attributeName = "DUNS_NO")
  public String getDuns_no()
  {
    return duns_no;
  }

  /**
   * @param duns_no
   */
  public void setDuns_no( String duns_no )
  {
    this.duns_no = duns_no;
  }


}
