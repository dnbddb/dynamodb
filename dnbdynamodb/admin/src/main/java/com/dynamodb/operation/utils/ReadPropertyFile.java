package com.dynamodb.operation.utils;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dynamodb.operation.constant.SchemaDefinationsConstants;


/**
 * <p>
 * This class for reading the property file
 * </p>
 * @author impadmin
 * @since 08-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class ReadPropertyFile
{

  public static String source_table;
  public static String save_export_dir;
  public static String export_filename;
  public static String credential_filename;
  public static String createspecificTable_filename;
  public static String createAllTables_filename;
  public static String exp_source_table;
  public static String env;
  public static String sourceTable;
  public static String targetTable;
  public static String presourceTable;
  public static String pretargetTable;
  public static String proxyFlag;
  public static String proxyHost;
  public static String proxyPort;
  public static String auditHeader;
  public static String region;
  public static String auditTable;
  public static String auditTableWithoutEnv;

  private static Logger LOGGER = LoggerFactory.getLogger( ReadPropertyFile.class );

  static {

    try {
      LOGGER.info( "Going to read the DB property file" );
      GetPropertyValues properties = new GetPropertyValues( "cfg/configuration.properties" );     
      save_export_dir = properties.getPropValues( "EXPORT_OUTPUT_DIR" );
      export_filename = properties.getPropValues( "EXPORT_FILE_NAME" );
      credential_filename = properties.getPropValues( "CREDENTIAL_FILE_NAME" );
      createAllTables_filename = properties.getPropValues( "CREATE_ALL_TABLE_PATH" );
      if ( null != properties.getPropValues( "ENV" ) ) {
        env = properties.getPropValues( "ENV" ).toUpperCase();
      }
      sourceTable = properties.getPropValues( "EXPORT_TABLE" );
      targetTable = properties.getPropValues( "ARCHIVE_TABLE" );
      presourceTable = sourceTable;
      pretargetTable = targetTable;
      proxyFlag = properties.getPropValues( "proxyFlag" );
      proxyHost = properties.getPropValues( "proxyHost" );
      proxyPort = properties.getPropValues( "proxyPort" );      
      auditHeader = properties.getPropValues( "DNB_CIRRUS_FEED_AUDIT" );
      region = properties.getPropValues( "REGION" );
      auditTableWithoutEnv = SchemaDefinationsConstants.AUDIT_MONITOR;

      if ( null != ReadPropertyFile.env && !ReadPropertyFile.env.equals( "" ) ) {
        LOGGER.info( "Environment is " + env );
        sourceTable = sourceTable + "_" + env;
        targetTable = targetTable + "_" + env;
        auditTable = SchemaDefinationsConstants.AUDIT_MONITOR +"_"+env;
        
      } else {        
        LOGGER.info( "Environment is PROD " );
      }
      LOGGER.info( "Reading config file values are->" + save_export_dir );
      LOGGER.info( "Reading config file Export_filename values are->" + export_filename );
      LOGGER.info( "Reading config file credential_filename values are->" + credential_filename );
      LOGGER.info( "Reading config file createAllTables_filename values are->" + createAllTables_filename );
      LOGGER.info( "Reading config file Environment values are->" + env );
      LOGGER.info( "Reading config file sourceTable values are->" + sourceTable );
      LOGGER.info( "Reading config file targetTable values are->" + targetTable );
      LOGGER.info( "Reading config file proxyFlag values are->" + proxyFlag );
      LOGGER.info( "Reading config file proxyHost values are->" + proxyHost );
      LOGGER.info( "Reading config file proxyPort values are->" + proxyPort );
      LOGGER.info( "Reading config file auditHeader values are->" + auditHeader );

    } catch( Exception e ) {
      LOGGER.error( "Error in reading property file." + e );
    }
  }

  /**
   * <p>
   * </p>
   * @param args
   * @throws IOException
   */
  public static void main( String[] args )
    throws IOException
  {

  }
}
