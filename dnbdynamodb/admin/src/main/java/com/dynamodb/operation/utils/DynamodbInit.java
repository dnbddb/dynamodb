package com.dynamodb.operation.utils;

import java.io.File;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.dynamodb.operation.pojo.AdminDetails;
import com.dynamodb.operation.pojo.ProxyDetails;


/**
 * This class for initializing DynaMO DB
 * @author IMPETUS
 */

/**
 * <p>
 * This class is used for initializing DynamoDB
 * </p>
 * @author impadmin
 * @since 08-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class DynamodbInit
{

  protected static DynamoDB dynamo;
  public static AmazonDynamoDBClient client;
  private static Logger LOGGER = LoggerFactory.getLogger( DynamodbInit.class );
  private static ProxyDetails proxyDetail = AdminConfigHandler.getProxyDetails();
  private static AdminDetails adminDetails = AdminConfigHandler.getAdminDetails();

  /**
   * <p>
   * This method is used for setting the proxy depending on passed parameter and authentication. *
   * </p>
   * @return DynamoDB
   * @throws InterruptedException
   */
  public static DynamoDB setup()
    throws InterruptedException
  {

    if ( null != proxyDetail.getProxyFlag() && proxyDetail.getProxyFlag().equalsIgnoreCase( "true" ) ) {
      LOGGER.info( "Going to set proxy." );
      Properties props = System.getProperties();
      props.setProperty( "http.proxyHost", proxyDetail.getProxyHost().trim() );
      props.setProperty( "https.proxyPort", proxyDetail.getProxyPort().trim() );
      ClientConfiguration cconfig = new ClientConfiguration();
      cconfig.setProxyHost( props.getProperty( "http.proxyHost" ) );
      cconfig.setProxyPort( Integer.parseInt( props.getProperty( "https.proxyPort" ) ) );
      client = new AmazonDynamoDBClient( awsTestCredentials(), cconfig );
    } else {
      LOGGER.info( "Not setting proxy value ." );
      client = new AmazonDynamoDBClient( awsTestCredentials() );
    }
    String region = proxyDetail.getRegion();
    LOGGER.debug("Going to create table for region --->"+region); 
    if(null != region && ! region.equals( "" ))
    {      
       client.setRegion(Regions.fromName(region));      
    }       
    dynamo = new DynamoDB( client );
    return dynamo;
  }

  /**
   * <p>
   * This method is used for shuts down and release all resources.
   * </p>
   */
  public static void tearDown()
  {
    dynamo.shutdown();
  }

  /**
   * <p>
   * This method is used for authentication accessKey and secretKey which are configure in dbCredential.properties file.
   * </p>
   * @return
   */
  private static AWSCredentials awsTestCredentials()
  {
    try {
      LOGGER.info( "Credential file location -->" + adminDetails.getCredential_file_name() );
      return new PropertiesCredentials( new File( adminDetails.getCredential_file_name() ) );
    } catch( Exception e ) {
      LOGGER.info( "Error in authentication->" + e );
      throw new RuntimeException( e );
    }
  }
}
