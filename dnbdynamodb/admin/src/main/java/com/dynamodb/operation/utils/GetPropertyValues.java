package com.dynamodb.operation.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


/**
 * <p>
 * This class is used for reading the properties file
 * </p>
 * @author impadmin
 * @since 08-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class GetPropertyValues
{  
  private String TABLE_PRIMARY_KEY = "";
  private String propFileName = "";

  /**
   * <p>
   * Create a new instance of GetPropertyValues.
   * </p>
   */
  public GetPropertyValues()
  {

  }

  /**
   * <p>
   * Create a new instance of GetPropertyValues.
   * </p>
   * @param DB_ConfigFile
   */
  public GetPropertyValues( String DB_ConfigFile )
  {
    propFileName = DB_ConfigFile;
  }

  /**
   * <p>
   * This method is used for getting the property value based on the key.
   * </p>
   * @param TABLE_KEY_VALUE
   * @return String
   * @throws IOException
   */
  public String getPropValues( String TABLE_KEY_VALUE )
    throws IOException
  {
    Properties prop = new Properties();
    FileInputStream fileInputStream = new FileInputStream( new File( propFileName ) );
    if ( null != fileInputStream ) {
      prop.load( fileInputStream );
    }
    TABLE_PRIMARY_KEY = prop.getProperty( TABLE_KEY_VALUE );    
    return TABLE_PRIMARY_KEY;
  }
}