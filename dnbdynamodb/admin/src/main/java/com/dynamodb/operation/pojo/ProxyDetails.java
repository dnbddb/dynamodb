/**
 * ProxyDetails.java 07-May-2015
 */
package com.dynamodb.operation.pojo;

/**
 * <p>
 * A POJO class which holds the information of a Proxy settings (i.e proxyHost,proxyPort etc) which flume use to connect to DynamoDB. Value of
 * proxyDetails can be set from config.json available inside cfg/ folder.
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class ProxyDetails
{
  private String proxyFlag;
  private String proxyHost;
  private String proxyPort;
  private String region;

  /**
   * <p>
   * See {@link #setproxyFlag(String)}
   * </p>
   * @return Returns the proxyFlag.
   */
  public String getProxyFlag()
  {
    return proxyFlag;
  }

  /**
   * <p>
   * Set the value of <code>proxyFlag</code>.
   * </p>
   * @param proxyFlag
   *          The proxyFlag to set.
   */
  public void setProxyFlag( String proxyFlag )
  {
    this.proxyFlag = proxyFlag;
  }

  /**
   * <p>
   * See {@link #setproxyHost(String)}
   * </p>
   * @return Returns the proxyHost.
   */
  public String getProxyHost()
  {
    return proxyHost;
  }

  /**
   * <p>
   * Set the value of <code>proxyHost</code>.
   * </p>
   * @param proxyHost
   *          The proxyHost to set.
   */
  public void setProxyHost( String proxyHost )
  {
    this.proxyHost = proxyHost;
  }

  /**
   * <p>
   * See {@link #setproxyPort(String)}
   * </p>
   * @return Returns the proxyPort.
   */
  public String getProxyPort()
  {
    return proxyPort;
  }

  /**
   * <p>
   * Set the value of <code>proxyPort</code>.
   * </p>
   * @param proxyPort
   *          The proxyPort to set.
   */
  public void setProxyPort( String proxyPort )
  {
    this.proxyPort = proxyPort;
  }

  /**
   * <p>
   * See {@link #setregion(String)}
   * </p>
   * @return Returns the region.
   */
  public String getRegion()
  {
    return region;
  }

  /**
   * <p>
   * Set the value of <code>region</code>.
   * </p>
   * @param region The region to set.
   */
  public void setRegion( String region )
  {
    this.region = region;
  }
}
