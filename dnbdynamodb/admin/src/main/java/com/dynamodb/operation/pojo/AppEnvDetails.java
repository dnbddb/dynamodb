package com.dynamodb.operation.pojo;

/**
 * <p>
 * A POJO class which holds the information of a Environment (i.e DEV,QA,PROD) details where flume is running. Value of env can be set from
 * config.json available inside cfg/ folder.
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class AppEnvDetails
{

  private String env;
  private String flumeEventlogs;

  /**
   * <p>
   * See {@link #setenv(String)}
   * </p>
   * @return Returns the env.
   */
  public String getEnv()
  {
    return env;
  }

  /**
   * <p>
   * Set the value of <code>env</code>.
   * </p>
   * @param env
   *          The env to set.
   */
  public void setEnv( String env )
  {
    this.env = env;
  }

  /**
   * <p>
   * See {@link #setflumeEventlogs(String)}
   * </p>
   * @return Returns the flumeEventlogs.
   */
  public String getFlumeEventlogs()
  {
    return flumeEventlogs;
  }

  /**
   * <p>
   * Set the value of <code>flumeEventlogs</code>.
   * </p>
   * @param flumeEventlogs The flumeEventlogs to set.
   */
  public void setFlumeEventlogs( String flumeEventlogs )
  {
    this.flumeEventlogs = flumeEventlogs;
  }

}