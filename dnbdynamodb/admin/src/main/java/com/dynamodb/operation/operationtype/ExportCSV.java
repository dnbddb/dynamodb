/**
 * 
 */
package com.dynamodb.operation.operationtype;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper.FailedBatch;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.dynamodb.operation.bean.CICInput;
import com.dynamodb.operation.bean.CICInputArchive;
import com.dynamodb.operation.constant.SchemaDefinationsConstants;
import com.dynamodb.operation.exception.DynamoDBException;
import com.dynamodb.operation.utils.AdminConfigHandler;
import com.dynamodb.operation.utils.DynamodbInit;

/**
 * <p>
 * This class is used for exporting operation through jmx *
 * </p>
 * @author IMPETUS
 * @since 08-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */

public class ExportCSV
  extends TimerTask
{

  private final String NEW_LINE = "\n";
  private DynamoDB dynamo = null;
  private static Logger logger = LoggerFactory.getLogger( ExportCSV.class );
  private static String envDetail = null;

  /**
   * <p>
   * Create a new instance of ExportCSV.
   * </p>
   */
  public ExportCSV()
  {
    try {
      dynamo = DynamodbInit.setup();
      logger.info( "Dynamo->" + dynamo );
    } catch( InterruptedException ie ) {
      logger.error( "Insert into catch block" );
    }
  }

  /*
   * This is override method which is used for scheduling the export job.
   * @see java.util.TimerTask#run()
   * @return void
   */
  public void run()
  {

    try {
      logger.info( "Start running  time at --->" + new Date() );
      String response_string = exportCSV();
      logger.info( "Retuning response is-->" + response_string );
      logger.info( "Finshed job at -->" + new Date() );
    } catch( Exception e ) {
      logger.error( "Insert into catch block." + e );
    }
  }

  /**
   * <p>
   * This is main method for calling export option directly *
   * </p>
   * @param args
   */
  public static void main( String[] args )
  {
    
  }

  /**
   * <p>
   * This method is used for exporting the data.
   * </p>
   * @return String
   * @throws IOException
   * @throws Exception
   */
  public String exportCSV()
    throws DynamoDBException
  {
    String msg = "SUCCESS";
    String DUNSNO = null;
    String CREATE_DATE = null;
    String COUNTRY = null;
    String RECORD_NO = null;
    envDetail = AdminConfigHandler.getAppEnvDetails().getEnv().trim().toUpperCase();
    ScanRequest scanRequest = null;
    FileWriter writer = null;
    List<CICInput> cicList = new ArrayList<CICInput>();
    Calendar calendar = Calendar.getInstance();
    String exportCICFile = AdminConfigHandler.getAdminDetails().getExport_output_dir() + File.separator
      + AdminConfigHandler.getAdminDetails().getExport_file_name() + new SimpleDateFormat( "yyyyMMddHHmmss" ).format( calendar.getTime() )
      + SchemaDefinationsConstants.EXPORT_FILE_NAME_EXT;

    logger.info( "Finally Export File name is--->" + AdminConfigHandler.getAdminDetails().getExport_file_name() );
    StringBuilder exportCSVData = new StringBuilder();
    exportCSVData.append( SchemaDefinationsConstants.CIC_CSV_HEADER_LINE );
    exportCSVData.append( NEW_LINE );

    if ( null != envDetail && !envDetail.equals( "" ) ) {
      logger.info( "Environment is " + envDetail );
      scanRequest = new ScanRequest().withTableName( AdminConfigHandler.getAdminDetails().getExport_table() + "_" + envDetail );
    } else {
      logger.info( "Environment is PROD " );
      scanRequest = new ScanRequest().withTableName( AdminConfigHandler.getAdminDetails().getExport_table() );
    }
    try {
      logger.info( "Scan Request: " + scanRequest );
      if ( null != DynamodbInit.client ) {
        ScanResult scanResponse = DynamodbInit.client.scan( scanRequest );

        if ( null != scanResponse ) {
          writer = new FileWriter( exportCICFile );
          writer.append( SchemaDefinationsConstants.CIC_CSV_HEADER_LINE );
          writer.append( NEW_LINE );

          for( Map<String, AttributeValue> item : scanResponse.getItems() ) {

            CICInput cic = new CICInput();
            StringBuilder showParam = new StringBuilder();

            if ( null != item.get( SchemaDefinationsConstants.CIC_DUNS_NO ) ) {
              DUNSNO = item.get( SchemaDefinationsConstants.CIC_DUNS_NO ).toString();
            }
            if ( null != item.get( SchemaDefinationsConstants.CIC_DATE_CREATED ) ) {
              CREATE_DATE = item.get( SchemaDefinationsConstants.CIC_DATE_CREATED ).toString();
            }
            if ( null != item.get( SchemaDefinationsConstants.CIC_COUNTRY ) ) {
              COUNTRY = item.get( SchemaDefinationsConstants.CIC_COUNTRY ).toString();
            }
            if ( null != item.get( SchemaDefinationsConstants.CIC_RECORD_NO ) ) {
              RECORD_NO = item.get( SchemaDefinationsConstants.CIC_RECORD_NO ).toString();
            }
            logger.debug( DUNSNO + " " + CREATE_DATE + " " + COUNTRY + " " + RECORD_NO );

            if ( null != DUNSNO && DUNSNO.length() >= 7 ) {
              String duns_no = DUNSNO.substring( DUNSNO.indexOf( ":" ) + 2, DUNSNO.lastIndexOf( "," ) );
              exportCSVData.append( duns_no + SchemaDefinationsConstants.COLUMN_DELIMITER );
              writer.append( duns_no + SchemaDefinationsConstants.COLUMN_DELIMITER );
              showParam.append( duns_no + SchemaDefinationsConstants.COLUMN_DELIMITER );
              cic.setDuns_no( duns_no );
            } else {
              exportCSVData.append( SchemaDefinationsConstants.COLUMN_DELIMITER );
              writer.append( SchemaDefinationsConstants.COLUMN_DELIMITER );
              showParam.append( SchemaDefinationsConstants.COLUMN_DELIMITER );
            }

            if ( null != CREATE_DATE && CREATE_DATE.length() >= 7 ) {
              String date_created = CREATE_DATE.substring( CREATE_DATE.indexOf( ":" ) + 2, CREATE_DATE.lastIndexOf( "," ) );
              exportCSVData.append( date_created + SchemaDefinationsConstants.COLUMN_DELIMITER );
              writer.append( date_created + SchemaDefinationsConstants.COLUMN_DELIMITER );
              showParam.append( date_created + SchemaDefinationsConstants.COLUMN_DELIMITER );
              cic.setDate_created( date_created );
            } else {
              exportCSVData.append( SchemaDefinationsConstants.COLUMN_DELIMITER );
              writer.append( SchemaDefinationsConstants.COLUMN_DELIMITER );
              showParam.append( SchemaDefinationsConstants.COLUMN_DELIMITER );
            }

            if ( null != COUNTRY && COUNTRY.length() >= 7 ) {
              String country = COUNTRY.substring( COUNTRY.indexOf( ":" ) + 2, COUNTRY.lastIndexOf( "," ) );
              exportCSVData.append( country + SchemaDefinationsConstants.COLUMN_DELIMITER );
              writer.append( country + SchemaDefinationsConstants.COLUMN_DELIMITER );
              showParam.append( country + SchemaDefinationsConstants.COLUMN_DELIMITER );
              cic.setCountry( country );
            } else {
              exportCSVData.append( SchemaDefinationsConstants.COLUMN_DELIMITER );
              writer.append( SchemaDefinationsConstants.COLUMN_DELIMITER );
              showParam.append( SchemaDefinationsConstants.COLUMN_DELIMITER );
            }

            if ( null != RECORD_NO && RECORD_NO.length() >= 7 ) {
              String record_no = RECORD_NO.substring( RECORD_NO.indexOf( ":" ) + 2, RECORD_NO.lastIndexOf( "," ) );
              if ( null != record_no ) {
                exportCSVData.append( record_no );
                writer.append( record_no );
                showParam.append( record_no );
                cic.setRecord_no( new Integer( record_no ) );
              }
            } else {
              exportCSVData.append( SchemaDefinationsConstants.COLUMN_DELIMITER );
              writer.append( SchemaDefinationsConstants.COLUMN_DELIMITER );
              showParam.append( SchemaDefinationsConstants.COLUMN_DELIMITER );
            }
            exportCSVData.append( NEW_LINE );
            writer.append( NEW_LINE );
            cicList.add( cic );
          }

        } else {
          logger.error( "Error in exporting table " + AdminConfigHandler.getAdminDetails().getExport_table() );
          throw new AmazonClientException( "Error in exporting table " + AdminConfigHandler.getAdminDetails().getExport_table() );
        }
      }

    } catch( AmazonServiceException e ) {
      logger.error( "Insert into catch block ->" + e );
      msg = "ERROR";
    } catch( AmazonClientException e ) {
      logger.error( "Insert into catch block ->" + e );
      msg = "ERROR";
    } catch( Exception e ) {
      logger.error( "Insert into catch block ->" + e );
      msg = "ERROR";
    }
    String result = insertDynamoDBData( cicList );
    // String result = "SUCCESS";
    logger.info( "Insert result is -->" + result );
    if ( null != result && result.equalsIgnoreCase( "SUCCESS" ) && msg.equalsIgnoreCase( "SUCCESS" ) && null != writer ) {
      try {
        writer.flush();
        writer.close();
      } catch( IOException e ) {
        logger.info( "Insert into catch block->" + e );
      }
      // deleteDynamoDBData( cicList );
    }
    return msg;
  }

  /**
   * <p>
   * This method is for inserting data in new archive table.
   * </p>
   * @param cicList
   * @return
   */
  private synchronized String insertDynamoDBData( List<CICInput> cicList )
  {
    String msg = "SUCCESS";
    logger.info( "Passed arrayList data..>" + cicList.size() );
    List<CICInputArchive> cicListNew = new ArrayList<CICInputArchive>();
    List<CICInput> oldcicList = new ArrayList<CICInput>();
    DynamoDBMapper mapper = new DynamoDBMapper( DynamodbInit.client );
    String tableName = AdminConfigHandler.getAdminDetails().getArchive_table();
    int batchSize = 0;

    if ( null != envDetail && !envDetail.equalsIgnoreCase( "" ) ) {
      tableName = tableName + "_" + envDetail;
    }
    try {
      if ( null != cicList && cicList.size() > 0 ) {
        CICInputArchive cicNew = null;
        for( Iterator<CICInput> iterator = cicList.iterator(); iterator.hasNext(); ) {
          CICInput cic = (CICInput)iterator.next();
          if ( batchSize == 100 ) {
            // logger.info( "Insert into saving the data. envDetail setup ->>"+envDetail );
            List<FailedBatch> failedBatchs = mapper.batchWrite( cicListNew, Collections.emptyList(), new DynamoDBMapperConfig(
              new DynamoDBMapperConfig.TableNameOverride( tableName ) ) );
            if ( failedBatchs.size() == 0 ) {
              deleteDynamoDBData( oldcicList );
            }
            cicNew = null;
            cicListNew = null;
            oldcicList = null;
            batchSize = 0;
            cicListNew = new ArrayList<CICInputArchive>();
            oldcicList = new ArrayList<CICInput>();

            cicNew = new CICInputArchive();
            cicNew.setDate_created( cic.getDate_created() );
            cicNew.setRecord_no( cic.getRecord_no() );
            cicNew.setDuns_no( cic.getDuns_no() );
            cicNew.setCountry( cic.getCountry() );
            cicListNew.add( cicNew );
            oldcicList.add( cic );
          } else {
            cicNew = new CICInputArchive();
            cicNew.setDate_created( cic.getDate_created() );
            cicNew.setRecord_no( cic.getRecord_no() );
            cicNew.setDuns_no( cic.getDuns_no() );
            cicNew.setCountry( cic.getCountry() );
            cicListNew.add( cicNew );
            oldcicList.add( cic );
          }
          batchSize++;
        }
        if ( null != cicListNew && null != oldcicList ) {
          List<FailedBatch> failedBatchs = mapper.batchWrite( cicListNew, Collections.emptyList(), new DynamoDBMapperConfig(
            new DynamoDBMapperConfig.TableNameOverride( tableName ) ) );
          if ( failedBatchs.size() == 0 ) {
            deleteDynamoDBData( oldcicList );
          }
          cicListNew = null;
          oldcicList = null;
        }
      }
    } catch( SecurityException e1 ) {
      logger.error( "Insert into SecurityException block." + e1 );
      msg = "ERROR";
    } catch( IllegalArgumentException e1 ) {
      logger.error( "Insert into IllegalArgumentException block." + e1 );
      msg = "ERROR";
    } catch( Exception e1 ) {
      logger.error( "Insert into Exception block." + e1 );
      msg = "ERROR";
    } finally {
      logger.info( "Insert into finally block." );
      mapper = null;
    }
    return msg;
  }

  /**
   * <p>
   * This method is for deleting the data from DNB_CIRRUS_CIC_INPUT table.
   * </p>
   * @param cicList
   * @return
   */
  private String deleteDynamoDBData( List<CICInput> cicList )
  {
    logger.debug( "Going to delete the record." );
    String msg = "SUCCESS";
    String tableName = AdminConfigHandler.getAdminDetails().getExport_table();
    logger.debug( " Insert into saving the data env detail  ->>" + envDetail );
    if ( null != envDetail && !envDetail.equalsIgnoreCase( "" ) ) {
      tableName = tableName + "_" + envDetail;
    }
    DynamoDBMapper mapper = new DynamoDBMapper( DynamodbInit.client );

    try {
      logger.debug( "Entering into deleteDynamoDBData() ...." + cicList.size() );
      mapper.batchWrite( Collections.emptyList(), cicList, new DynamoDBMapperConfig( new DynamoDBMapperConfig.TableNameOverride( tableName ) ) );
      logger.debug( "Exiting from deleteDynamoDBData()." );
    } catch( Exception e ) {
      msg = "ERROR";
      logger.error( "Insert into catch block." + e );
    }
    return msg;
  } 

}
