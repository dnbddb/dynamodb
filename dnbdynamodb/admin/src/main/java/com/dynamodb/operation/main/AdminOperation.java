package com.dynamodb.operation.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dynamodb.operation.exception.DynamoDBException;
import com.dynamodb.operation.operationtype.CreateTables;
import com.dynamodb.operation.utils.AdminConfigHandler;

/**
 * <p>
 * This class for is used for configure the operation create or export.
 * </p>
 * @author impadmin
 * @since 08-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class AdminOperation
{

  private static Logger LOGGER = LoggerFactory.getLogger( AdminOperation.class );

  /**
   * <p>
   * This method is used for controlling the operation either create table or jmx.
   * </p>
   * @param args
   * @throws Exception
   */
  public static void main( String[] args )
    throws Exception
  {
    int configStatus = AdminConfigHandler.loadConfig();
    if ( 0 != configStatus ) {
      System.exit( configStatus );
    }
    LOGGER.info( "Passed no of argument ->" + args.length );

    if ( args.length > 0 ) {
      if ( args[0].equalsIgnoreCase( "createtable" ) ) {
        CreateTables createTables = new CreateTables();
        createTables.createTable();
      } else if ( args[0].equalsIgnoreCase( "exporttable" ) ) {
        DNBIOperationMain.main( args );
      } else {
        LOGGER.info( "Please pass the parameter createtable or exporttable ." );
        throw new DynamoDBException( "Please pass the parameter createtable or exporttable ." );
      }
    } else {
      LOGGER.info( "Please pass the parameter." );
      throw new DynamoDBException( "Please pass the parameter." );
    }
    LOGGER.info( "Finished the execution of target class." );
  }
}
