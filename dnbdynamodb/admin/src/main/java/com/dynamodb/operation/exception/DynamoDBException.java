package com.dynamodb.operation.exception;

/**
 * <p>
 * This is the class for making custom exception
 * </p>
 * @author impadmin
 * @since 08-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class DynamoDBException
  extends Exception
{

  private static final long serialVersionUID = 1997753363232807009L;

  /**
   * <p>
   * Create a new instance of DynamoDBException.
   * </p>
   */
  public DynamoDBException()
  {
  }

  /**
   * <p>
   * Create a new instance of DynamoDBException.
   * </p>
   * @param message
   */
  public DynamoDBException( final String message )
  {
    super( message );
  }

  /**
   * <p>
   * Create a new instance of DynamoDBException.
   * </p>
   * @param th
   */
  public DynamoDBException( final Throwable th )
  {
    super( th );
  }

  /**
   * <p>
   * Create a new instance of DynamoDBException.
   * </p>
   * @param message
   * @param th
   */
  public DynamoDBException( final String message, final Throwable th )
  {
    super( message, th );
  }

  /**
   * <p>
   * Create a new instance of DynamoDBException.
   * </p>
   * @param message
   * @param e
   */
  public DynamoDBException( final String message, Exception e )
  {
    super( message, e );
  }

}
