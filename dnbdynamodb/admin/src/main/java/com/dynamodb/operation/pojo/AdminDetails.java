package com.dynamodb.operation.pojo;

public class AdminDetails
{  
  private String credential_file_name;
  private String create_all_table_path;
  private String export_output_dir;
  private String export_file_name;
  private String export_table;
  private String archive_table;
  private String dnb_cirrus_feed_audit;
  /**
   * <p>
   * See {@link #setcredential_file_name(String)}
   * </p>
   * @return Returns the credential_file_name.
   */
  public String getCredential_file_name()
  {
    return credential_file_name;
  }
  /**
   * <p>
   * Set the value of <code>credential_file_name</code>.
   * </p>
   * @param credential_file_name The credential_file_name to set.
   */
  public void setCredential_file_name( String credential_file_name )
  {
    this.credential_file_name = credential_file_name;
  }
  /**
   * <p>
   * See {@link #setcreate_all_table_path(String)}
   * </p>
   * @return Returns the create_all_table_path.
   */
  public String getCreate_all_table_path()
  {
    return create_all_table_path;
  }
  /**
   * <p>
   * Set the value of <code>create_all_table_path</code>.
   * </p>
   * @param create_all_table_path The create_all_table_path to set.
   */
  public void setCreate_all_table_path( String create_all_table_path )
  {
    this.create_all_table_path = create_all_table_path;
  }
  /**
   * <p>
   * See {@link #setexport_output_dir(String)}
   * </p>
   * @return Returns the export_output_dir.
   */
  public String getExport_output_dir()
  {
    return export_output_dir;
  }
  /**
   * <p>
   * Set the value of <code>export_output_dir</code>.
   * </p>
   * @param export_output_dir The export_output_dir to set.
   */
  public void setExport_output_dir( String export_output_dir )
  {
    this.export_output_dir = export_output_dir;
  }
  /**
   * <p>
   * See {@link #setexport_file_name(String)}
   * </p>
   * @return Returns the export_file_name.
   */
  public String getExport_file_name()
  {
    return export_file_name;
  }
  /**
   * <p>
   * Set the value of <code>export_file_name</code>.
   * </p>
   * @param export_file_name The export_file_name to set.
   */
  public void setExport_file_name( String export_file_name )
  {
    this.export_file_name = export_file_name;
  }
  /**
   * <p>
   * See {@link #setexport_table(String)}
   * </p>
   * @return Returns the export_table.
   */
  public String getExport_table()
  {
    return export_table;
  }
  /**
   * <p>
   * Set the value of <code>export_table</code>.
   * </p>
   * @param export_table The export_table to set.
   */
  public void setExport_table( String export_table )
  {
    this.export_table = export_table;
  }
  /**
   * <p>
   * See {@link #setarchive_table(String)}
   * </p>
   * @return Returns the archive_table.
   */
  public String getArchive_table()
  {
    return archive_table;
  }
  /**
   * <p>
   * Set the value of <code>archive_table</code>.
   * </p>
   * @param archive_table The archive_table to set.
   */
  public void setArchive_table( String archive_table )
  {
    this.archive_table = archive_table;
  }
  /**
   * <p>
   * See {@link #setdnb_cirrus_feed_audit(String)}
   * </p>
   * @return Returns the dnb_cirrus_feed_audit.
   */
  public String getDnb_cirrus_feed_audit()
  {
    return dnb_cirrus_feed_audit;
  }
  /**
   * <p>
   * Set the value of <code>dnb_cirrus_feed_audit</code>.
   * </p>
   * @param dnb_cirrus_feed_audit The dnb_cirrus_feed_audit to set.
   */
  public void setDnb_cirrus_feed_audit( String dnb_cirrus_feed_audit )
  {
    this.dnb_cirrus_feed_audit = dnb_cirrus_feed_audit;
  }
  
  
  
}
