package com.dynamodb.operation.operationtype;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;
import com.amazonaws.services.dynamodbv2.model.TableDescription;
import com.dynamodb.operation.constant.SchemaDefinationsConstants;
import com.dynamodb.operation.exception.DynamoDBException;
import com.dynamodb.operation.utils.AdminConfigHandler;
import com.dynamodb.operation.utils.DynamodbInit;

/**
 * <p>
 * This class is used for creating the tables which are configure in createtable.xml files.
 * </p>
 * @author IMPETUS
 * @since 08-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class CreateTables
{

  private TableWriteItems tableWriteItems = null;
  private String TABLE_HASH_KEY_VALUE = null;
  private String TABLE_RANGE_KEY_VALUE = null;
  private Long READ_CAPACITY_UNIT = new Long( 1 );
  private Long WRITE_CAPACITY_UNIT = new Long( 1 );
  private ProvisionedThroughput THRUPUT = null;
  private static DynamoDB dynamo = null;
  private static String envDetail = null;

  private static Logger LOGGER = LoggerFactory.getLogger( CreateTables.class );


  /**
   * <p>
   * Create a new instance of CreateTables which are calling setup() method from DynamodbInit .
   * </p>
   */
  public CreateTables()
  {
    try {
      dynamo = DynamodbInit.setup();
    } catch( InterruptedException ie ) {

    }
  }

  /**
   * <p>
   * This is main method for calling createtables
   * </p>
   * @param args
   */
  public static void main( String[] args )
  {
    
  }

  /**
   * <p>
   * This method is used for creating the tables which are configure in createtable.xml file.
   * </p>
   * @return
   * @throws DynamoDBException
   */
  public String createTable()
    throws DynamoDBException
  {

    String msg = "SUCCESS";
    String DB_TABLE_NAME = null;
    String DB_ERROR_TABLE_NAME = null;
    envDetail = AdminConfigHandler.getAppEnvDetails().getEnv().trim().toUpperCase();

    try {
      LOGGER.info( "Going to create the tables." );
      LOGGER.info( "Environment is->" + envDetail );
      File tableNames = new File( AdminConfigHandler.getAdminDetails().getCreate_all_table_path() );
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse( tableNames );
      doc.getDocumentElement().normalize();
      NodeList nodes = doc.getElementsByTagName( "table" );

      for( int i = 0; i < nodes.getLength(); i++ ) {

        String TABLE_PRIMARY_KEY_HASH = null;
        String TABLE_PRIMARY_KEY_RANGE = null;
        String readCapacityStringValue = null;
        String writeCapacityStringValue = null;
        Table error_Table = null;
        DB_ERROR_TABLE_NAME = null;
        DB_TABLE_NAME = null;
        TABLE_HASH_KEY_VALUE = null;
        TABLE_RANGE_KEY_VALUE = null;

        Node node = nodes.item( i );

        if ( null != node && node.getNodeType() == Node.ELEMENT_NODE ) {
          Element element = (Element)node;

          try {

            DB_TABLE_NAME = getValue( "TABLE", element );

            if ( null != envDetail ) {
              if ( ( null != DB_TABLE_NAME )
                && !( DB_TABLE_NAME.equalsIgnoreCase( AdminConfigHandler.getAdminDetails().getExport_table() )
                  || DB_TABLE_NAME.equalsIgnoreCase( AdminConfigHandler.getAdminDetails().getArchive_table() ) || DB_TABLE_NAME.equalsIgnoreCase( SchemaDefinationsConstants.AUDIT_MONITOR ) ) ) {
                DB_ERROR_TABLE_NAME = DB_TABLE_NAME;
              }
            }
            if ( null != DB_TABLE_NAME && null != envDetail && !envDetail.equals( "" ) ) {
              LOGGER.info( "Environment is " + envDetail );
              DB_TABLE_NAME = DB_TABLE_NAME + "_" + envDetail;
            } else {
              LOGGER.info( "Environment is PROD " );
            }

            if ( null != DB_ERROR_TABLE_NAME && DB_ERROR_TABLE_NAME.length() > 0 ) {
              if ( null != envDetail && !envDetail.equals( "" ) ) {
                DB_ERROR_TABLE_NAME = DB_ERROR_TABLE_NAME + "_ERROR_" + envDetail;
              } else {
                DB_ERROR_TABLE_NAME = DB_ERROR_TABLE_NAME + "_ERROR";
              }
            }

            TABLE_PRIMARY_KEY_HASH = getValue( "PRIMARY_KEY_HASH", element );
            TABLE_PRIMARY_KEY_RANGE = getValue( "PRIMARY_KEY_RANGE", element );
            readCapacityStringValue = getValue( "READ_CAPACITY", element );
            writeCapacityStringValue = getValue( "WRITE_CAPACITY", element );
          } catch( Exception e1 ) {
            msg = "ERROR";
            LOGGER.error( "Insert into catch block." + e1 );
            throw new DynamoDBException( "Error in getting column values for table " + DB_TABLE_NAME );
          }

          if ( ( null != TABLE_PRIMARY_KEY_HASH )
            && !( TABLE_PRIMARY_KEY_HASH.equals( "," ) || TABLE_PRIMARY_KEY_HASH.equals( ":" ) || TABLE_PRIMARY_KEY_HASH.equals( ";" ) )
            && !( TABLE_PRIMARY_KEY_HASH.equalsIgnoreCase( "null" ) ) && !( TABLE_PRIMARY_KEY_HASH.equals( "" ) ) ) {
            TABLE_HASH_KEY_VALUE = TABLE_PRIMARY_KEY_HASH;
          } else {
            msg = "ERROR";
            throw new DynamoDBException( "Error in setting primary key for table " + DB_TABLE_NAME );
          }

          if ( TABLE_PRIMARY_KEY_RANGE != null
            && !( TABLE_PRIMARY_KEY_RANGE.equals( "," ) || TABLE_PRIMARY_KEY_RANGE.equals( ":" ) || TABLE_PRIMARY_KEY_RANGE.equals( ";" ) )
            && !TABLE_PRIMARY_KEY_RANGE.equalsIgnoreCase( "null" ) && !( TABLE_PRIMARY_KEY_RANGE.equals( "" ) ) ) {
            TABLE_RANGE_KEY_VALUE = TABLE_PRIMARY_KEY_RANGE;
          }

          if ( null != readCapacityStringValue && !( readCapacityStringValue.equals( "0" ) ) ) {

            Long readCapacityValue = new Long( readCapacityStringValue );
            if ( readCapacityValue > 0 ) {
              READ_CAPACITY_UNIT = readCapacityValue;
            }
          }

          if ( null != writeCapacityStringValue && !( writeCapacityStringValue.equals( "0" ) ) ) {

            Long writeCapacityValue = new Long( writeCapacityStringValue );
            if ( writeCapacityValue > 0 ) {
              WRITE_CAPACITY_UNIT = writeCapacityValue;
            }
          }
          THRUPUT = new ProvisionedThroughput( READ_CAPACITY_UNIT, WRITE_CAPACITY_UNIT );
          tableWriteItems = new TableWriteItems( DB_TABLE_NAME );
          Table table = dynamo.getTable( DB_TABLE_NAME );
          // if ( ( null != DB_ERROR_TABLE_NAME ) && !( DB_ERROR_TABLE_NAME.equalsIgnoreCase( ReadPropertyFile.presourceTable ) ||
          // DB_ERROR_TABLE_NAME.equalsIgnoreCase( ReadPropertyFile.pretargetTable ) ) ) {
          if ( ( null != DB_ERROR_TABLE_NAME ) ) {
            tableWriteItems = new TableWriteItems( DB_ERROR_TABLE_NAME );
            error_Table = dynamo.getTable( DB_ERROR_TABLE_NAME );
          }
          LOGGER.info( "tableWriteItems " + tableWriteItems );
          // check if table already exists, and if so wait for it to become active
          TableDescription desc = table.waitForActiveOrDelete();
          LOGGER.info( "Table " + DB_TABLE_NAME + " is ->" + desc );
          if ( desc != null ) {
            LOGGER.info( "Skip creating table " + DB_TABLE_NAME + " which already exists and ready for use: " + desc );
            continue;
          }
          // Table doesn't exist. Let's create it.
          try {

            table = dynamo.createTable( newCreateTableRequest( DB_TABLE_NAME ) );
            if ( null != DB_ERROR_TABLE_NAME ) {
              error_Table = dynamo.createTable( newCreateTableRequest( DB_ERROR_TABLE_NAME ) );
            }

          } catch( Exception e ) {
            msg = "ERROR";
            LOGGER.error( "Insert into exception bblock ->" + e );

          }
          // Wait for the table to become active
          desc = table.waitForActive();
          if ( null != error_Table ) {
            desc = error_Table.waitForActive();
          }
          TABLE_HASH_KEY_VALUE = null;
          TABLE_RANGE_KEY_VALUE = null;
          DB_ERROR_TABLE_NAME = null;
          DB_TABLE_NAME = null;
        }
      }
    } catch( IOException e ) {
      LOGGER.error( "Insert into IOException block ->" + e );
      msg = "ERROR";
    } catch( DynamoDBException e1 ) {
      LOGGER.error( "Insert into DynamoDBException block ->" + e1 );
      LOGGER.error( "Errors ->" + e1 );
      msg = "ERROR";
    } catch( Exception e1 ) {
      LOGGER.error( "Insert into exception block ->" + e1 );
      LOGGER.error( "Errors ->" + e1 );
      msg = "ERROR";
    } finally {
      LOGGER.error( "Releasing the connection and resources ->" );
      DynamodbInit.tearDown();
    }

    if ( msg.equals( "ERROR" ) ) {
      LOGGER.info( "Error in  creating tables." );
    } else {
      LOGGER.info( "Table created successfully...." );
    }

    return msg;
  }

  /**
   * <p>
   * This method used for creating tables in dynamoDB
   * </p>
   * @param tableName
   * @return
   */
  private CreateTableRequest newCreateTableRequest( String tableName )
  {
    CreateTableRequest req = null;
    if ( null != TABLE_HASH_KEY_VALUE && TABLE_RANGE_KEY_VALUE == null ) {
      req = new CreateTableRequest().withTableName( tableName )
        .withAttributeDefinitions( new AttributeDefinition( TABLE_HASH_KEY_VALUE, ScalarAttributeType.S ) )
        .withKeySchema( new KeySchemaElement( TABLE_HASH_KEY_VALUE, KeyType.HASH ) )
        .withProvisionedThroughput( THRUPUT );
    } else if ( null != TABLE_HASH_KEY_VALUE && null != TABLE_RANGE_KEY_VALUE ) {
      if ( null != tableName
        && ( tableName.equalsIgnoreCase( SchemaDefinationsConstants.AUDIT_MONITOR + "_" + envDetail ) || ( tableName.equalsIgnoreCase( SchemaDefinationsConstants.AUDIT_MONITOR ) ) ) ) {
        LOGGER.info( "Going to create table with String and String option." );
        req = new CreateTableRequest().withTableName( tableName )
          .withAttributeDefinitions( new AttributeDefinition( TABLE_HASH_KEY_VALUE, ScalarAttributeType.S ) )
          .withKeySchema( new KeySchemaElement( TABLE_HASH_KEY_VALUE, KeyType.HASH ) )
          .withAttributeDefinitions( new AttributeDefinition( TABLE_RANGE_KEY_VALUE, ScalarAttributeType.S ) )
          .withKeySchema( new KeySchemaElement( TABLE_RANGE_KEY_VALUE, KeyType.RANGE ) )
          .withProvisionedThroughput( THRUPUT );
      } else {
        LOGGER.info( "Going to create table with String and Number option." );
        req = new CreateTableRequest().withTableName( tableName )
          .withAttributeDefinitions( new AttributeDefinition( TABLE_HASH_KEY_VALUE, ScalarAttributeType.S ) )
          .withKeySchema( new KeySchemaElement( TABLE_HASH_KEY_VALUE, KeyType.HASH ) )
          .withAttributeDefinitions( new AttributeDefinition( TABLE_RANGE_KEY_VALUE, ScalarAttributeType.N ) )
          .withKeySchema( new KeySchemaElement( TABLE_RANGE_KEY_VALUE, KeyType.RANGE ) )
          .withProvisionedThroughput( THRUPUT );
      }
    }
    return req;
  }

  /**
   * <p>
   * This method is used for getting the table xml tag attribute values.
   * </p>
   * @param tag
   * @param element
   * @return
   */
  private String getValue( String tag, Element element )
  {
    NodeList nodes = element.getElementsByTagName( tag ).item( 0 ).getChildNodes();
    Node node = (Node)nodes.item( 0 );
    if ( null != node ) {
      return node.getNodeValue();
    } else {
      return null;
    }
  }
}
