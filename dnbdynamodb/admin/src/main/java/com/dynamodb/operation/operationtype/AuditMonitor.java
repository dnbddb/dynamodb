package com.dynamodb.operation.operationtype;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ComparisonOperator;
import com.amazonaws.services.dynamodbv2.model.Condition;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.dynamodb.operation.constant.SchemaDefinationsConstants;
import com.dynamodb.operation.exception.DynamoDBException;
import com.dynamodb.operation.utils.AdminConfigHandler;
import com.dynamodb.operation.utils.DynamodbInit;

/**
 * <p>
 * This class is used for creating the tables which are configure in createtable.xml files.
 * </p>
 * @author IMPETUS
 * @since 08-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class AuditMonitor
{
  private final String NEW_LINE = "\n";
  private DynamoDB dynamo = null;
  private static Logger LOGGER = LoggerFactory.getLogger( AuditMonitor.class );
  

  /**
   * <p>
   * Create a new instance of ExportCSV.
   * </p>
   */
  public AuditMonitor()
  {
    try {     
      dynamo = DynamodbInit.setup();
      LOGGER.info( "Dynamo Object->"+dynamo );
    } catch( InterruptedException ie ) {
      LOGGER.error( "Insert into catch block" );
    }
  }


  /**
   * <p>
   * This is main method for calling export option directly *
   * </p>
   * @param args
   */
  public static void main( String[] args )
  {
    
  }

  public String auditMonitor( String fromDate, String toDate )
    throws DynamoDBException
  {
    LOGGER.info( "Calling audit monitor method()." );
    LOGGER.info( "Passed From Date  ->" + fromDate );
    LOGGER.info( "Passed To Date  ->" + toDate ); 
    ScanRequest scanRequest = null;
    String headerArr[] = null;
    String envDetail = null;
    String auditHeader = null;
    String sourceTable = null;    
    envDetail = AdminConfigHandler.getAppEnvDetails().getEnv().trim().toUpperCase();
    LOGGER.debug( "Table Name is -->" + SchemaDefinationsConstants.AUDIT_MONITOR + "_" + envDetail );
    StringBuffer auditstringBuffer = new StringBuffer();
    if ( null != envDetail && !envDetail.equals( "" ) ) {
      LOGGER.info( "Environment is " + envDetail );
      scanRequest = new ScanRequest().withTableName( SchemaDefinationsConstants.AUDIT_MONITOR + "_" + envDetail );
    } else {
      LOGGER.info( "Environment is PROD " );
      scanRequest = new ScanRequest().withTableName( SchemaDefinationsConstants.AUDIT_MONITOR );
    }

    if ( null != DynamodbInit.client ) {
      LOGGER.debug( "Scan Table name is -------------->" + scanRequest );

      HashMap<String, Condition> scanFilter = new HashMap<String, Condition>();

      if ( ( null != fromDate && !fromDate.equals( "" ) ) && ( null != toDate && !toDate.equals( "" ) ) ) {
        Condition condition = new Condition().withComparisonOperator( ComparisonOperator.BETWEEN.toString() ).withAttributeValueList(
          new AttributeValue().withS( fromDate ), new AttributeValue().withS( toDate ) );
        scanFilter.put( "PROCESSED_DATE", condition );
      }

      else if ( ( null != fromDate && !fromDate.equals( "" ) ) ) {
        Condition condition1 = new Condition().withComparisonOperator( ComparisonOperator.EQ.toString() ).withAttributeValueList(
          new AttributeValue().withS( fromDate ) );
        scanFilter.put( "PROCESSED_DATE", condition1 );
      }
      scanRequest.withScanFilter( scanFilter );
      ScanResult scanResponse = DynamodbInit.client.scan( scanRequest );

      if ( null != scanResponse ) {
        auditHeader = AdminConfigHandler.getAdminDetails().getDnb_cirrus_feed_audit().trim();
        sourceTable = AdminConfigHandler.getAdminDetails().getExport_table();

        if ( null != auditHeader ) {
          headerArr = auditHeader.split( "," );

          if ( null != headerArr && headerArr.length > 0 ) {
            for( String headerValue : headerArr ) {
              if ( null != headerValue ) {
                for( int i = headerValue.length(); i < 35; i++ ) {
                  headerValue = headerValue + " ";
                }
                auditstringBuffer.append( headerValue + "\t" );
              }
            }
          }
        }
        auditstringBuffer.append( NEW_LINE );

        for( Map<String, AttributeValue> item : scanResponse.getItems() ) {
          String keyValue = null;
          for( String headerVal : headerArr ) {
            if ( null != item.get( headerVal ) ) {
              keyValue = item.get( headerVal ).toString();
            }
            if ( null != keyValue && keyValue.length() >= 7 ) {
              String attributValue = keyValue.substring( keyValue.indexOf( ":" ) + 2, keyValue.lastIndexOf( "," ) );

              if ( null != attributValue ) {
                for( int i = attributValue.length(); i < 35; i++ ) {
                  attributValue = attributValue + " ";
                }
              }
              auditstringBuffer.append( attributValue + "\t" );
              LOGGER.debug( "Adding attribut values is-->" + attributValue );
            }

            else {
              auditstringBuffer.append( "                                   " );
            }
          }
          auditstringBuffer.append( "\n" );
        }

      } else {
        LOGGER.debug( "Error in exporting table " + sourceTable );
        throw new AmazonClientException( "Error in exporting table " + sourceTable );
      }
    }
    return auditstringBuffer.toString();
  }
  
}


