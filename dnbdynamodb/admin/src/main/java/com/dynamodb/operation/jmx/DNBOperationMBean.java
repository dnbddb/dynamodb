package com.dynamodb.operation.jmx;

import com.dynamodb.operation.exception.DynamoDBException;




/**
 * <p>
 * This class for declaring the jmx operations.
 * </p>
 * @author impadmin
 * @since 08-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public interface DNBOperationMBean
{

  /**
   * <p>
   * This method is used for exporting csv data at any time
   * </p>
   * @return String
   * @throws Exception
   */
  public String exportNow()
    throws Exception;

  /**
   * <p>
   * This method is used for scheduling exporting option with the given parameters.
   * </p>
   * @param schedulDate
   * @param period
   * @return String
   * @throws Exception
   */
  public String exportSchedule( String schedulDate, long period )
    throws Exception;

  /**
   * <p>
   * This method is used for cancel previous schedule job
   * </p>
   * @return
   * @throws Exception
   */
  public String exportSchedulerOff()
    throws Exception;
  
  /**
   * <p>
   * 
   * </p>
   * @return
   * @throws com.dynamodb.operation.exception.DynamoDBException
   */
  public String auditMonitor(String fromDate, String toDate)
    throws DynamoDBException;
}
