package com.dynamodb.operation.main;

import java.io.IOException;
import java.io.InputStream;
import java.lang.management.ManagementFactory;
import org.apache.commons.modeler.Registry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.dynamodb.operation.jmx.impl.DNBOperationBeanImpl;


/**
 * <p>
 * This class is used to demonstrate use of MBeans with Apache
 * </p>
 * @author impadmin
 * @since 08-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class DNBIOperationMain
{
  private static Logger LOGGER = LoggerFactory.getLogger( DNBIOperationMain.class );

  // private static org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(DNBIOperationMain.class);

  /**
   * <p>
   * Main functionality for using Modeler XML metadata to instantiate and register a Model MBean with the Platform MBean Server.
   * </p>
   */
  public void applyCommonsModeler()
  {
    final String modelerMetadataFile = "operation_metadata.xml";
    DNBOperationBeanImpl dnbOperationBeanImpl = new DNBOperationBeanImpl();
    Registry registry = null;
    final InputStream modelerXmlInputStream = DNBIOperationMain.class.getClassLoader().getResourceAsStream( modelerMetadataFile );
    registry = Registry.getRegistry( null, null );
    registry.setMBeanServer( ManagementFactory.getPlatformMBeanServer() );
    try {
      registry.loadMetadata( modelerXmlInputStream );
      registry.registerComponent(
        dnbOperationBeanImpl, "com.dynamodb.operation.jmx:type=DNBOperationBean", "com.dynamodb.operation.jmx.impl.DNBOperationBeanImpl" );
      LOGGER.info( "Register MBean successfully." );
    } catch( IOException ioEx ) {
      LOGGER.error( "insert into catch block ->" + ioEx );
    } catch( Exception ex ) {
      LOGGER.error( "ERROR trying to access metadata file " + modelerMetadataFile + ":\n" + ex.getMessage() );
      LOGGER.error( "insert into catch block ->" + ex );
    }
  }

  /**
   * <p>
   * This method is used for Modeler MBean with meta data.
   * </p>
   * @param arguments
   * @throws Exception
   */
  public static void main( final String[] arguments )
    throws Exception
  {
    LOGGER.info( "Start the execution --->" );
    DNBIOperationMain me = new DNBIOperationMain();
    me.applyCommonsModeler();
    LOGGER.info( "Jmx is ready for operation--->" );
    Thread.sleep( Long.MAX_VALUE );
  }
}