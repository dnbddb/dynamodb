/*
 * Hello.java - MBean implementation for the Hello MBean. This class must implement all the Java methods declared in the HelloMBean interface, with
 * the appropriate behavior for each one.
 */

package com.dynamodb.operation.jmx.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

import javax.management.AttributeChangeNotification;
import javax.management.MBeanNotificationInfo;
import javax.management.NotificationBroadcasterSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dynamodb.operation.exception.DynamoDBException;
import com.dynamodb.operation.jmx.DNBOperationMBean;
import com.dynamodb.operation.operationtype.AuditMonitor;
import com.dynamodb.operation.operationtype.CreateTables;
import com.dynamodb.operation.operationtype.ExportCSV;

/**
 * <p>
 * This class is used for implementing export operations, which will shown through jmx UI.
 * </p>
 * @author impadmin
 * @since 08-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class DNBOperationBeanImpl
  extends NotificationBroadcasterSupport
  implements DNBOperationMBean
{

  CreateTables createTables = new CreateTables();
  ExportCSV exportCSV = new ExportCSV();
  AuditMonitor auditMonitor = new AuditMonitor();

  private static Logger LOGGER = LoggerFactory.getLogger( DNBOperationBeanImpl.class );
  Timer time = null;

  /**
   * This method is used for exporting csv data at any time
   * @return String
   * @throws InterruptedException
   *           *
   * @see com.dynamodb.operation.jmx.DNBOperationMBean#exportNow()
   */
  public String exportNow()
    throws Exception
  {
    String text = exportCSV.exportCSV();
    return text;
  }


  /**
   * This method is used canceling exporting schedule job.
   * @return String
   * @throws InterruptedException
   * @see com.dynamodb.operation.jmx.DNBOperationMBean#exportSchedulerOff()
   */
  public String exportSchedulerOff()
    throws Exception
  {
    LOGGER.info( "Inserting into exportSchedulerOff()." );
    String msg = null;
    if ( time != null ) {
      msg = "Cancell the previous scheduled job.";
      LOGGER.info( "Cancelled the previous schedule job." );
      time.cancel();
    } else {
      msg = "There are no job scheduled job.";
      LOGGER.info( "There are no job scheduled previously." );
    }
    LOGGER.info( "Exiting from exportSchedulerOff()." );
    return msg;
  }

  /**
   * This method is used for exporting data at scheduled intervals.
   * @return String
   * @throws InterruptedException
   * @see com.dynamodb.operation.jmx.DNBOperationMBean#exportSchedule(java.lang.String, long)
   */
  public String exportSchedule( String schedulDate, long period )
    throws Exception
  {
    String msg = "Export job is scheduled at ";
    Date date = null;
    LOGGER.info( "Going to export with time option." );

    ExportCSV exportCSV = new ExportCSV();
    try {
      String finalDateString = null;
      // SimpleDateFormat formatter = new SimpleDateFormat( "MMddyyyy HHmmss" );
      SimpleDateFormat formatter = new SimpleDateFormat( "yyyyMMdd HHmmss" );
      if ( null != schedulDate ) {
        finalDateString = formatDate( schedulDate );
      }
      LOGGER.info( "After formatting scheduling date is ---->" + finalDateString );
      date = formatter.parse( finalDateString );
      msg = msg + finalDateString;
      if ( period == 0 ) {
        period = 86400000;
      } else {
        period = period * 3600 * 1000;
      }
      LOGGER.info( "Before setting the period value is -->" + period );
      if ( time != null ) {
        LOGGER.info( "Start the  job 2nd time changing time." );
        time.cancel();
      }
      time = new Timer();
      time.schedule( exportCSV, date, period );
    } catch( Exception e ) {
      msg = "ERROR";
      LOGGER.info( "Insert into catch block -->" + e );
    }
    return msg;
  }


  public String auditMonitor( String fromDate, String toDate )
    throws DynamoDBException
  {
    String result = "";
    
    if( (null == fromDate || fromDate.equals( "" ) || fromDate.equalsIgnoreCase( "String" ) )  && (null == toDate || toDate.equals( "" ) || toDate.equalsIgnoreCase( "String" ) ))
    {
      LOGGER.info( "Calling audit monitor method() and checking the if condition 1." );
      result = "Please enter the Start Date and End Date.";
    }
    else if( !(null == fromDate || fromDate.equals( "" ) || fromDate.equalsIgnoreCase( "String" ) )  && (null == toDate || toDate.equals( "" ) || toDate.equalsIgnoreCase( "String" ) ))
    {
      LOGGER.info( "Calling audit monitor method() and checking the if condition 2 ." );
      result = "Please enter the End Date.";
    }
    else if( (null == fromDate || fromDate.equals( "" ) || fromDate.equalsIgnoreCase( "String" ) )  && !(null == toDate || toDate.equals( "" ) || toDate.equalsIgnoreCase( "String" ) ) )
    {
      LOGGER.info( "Calling audit monitor method() and checking the if condition 3." );
      result = "Please enter the Start Date.";
    }
    else{
      LOGGER.info( "Calling audit monitor method() and checking the else condition." );
      result = auditMonitor.auditMonitor( fromDate.trim(), toDate.trim() );     
    }            
    LOGGER.info( "Audit result is ->" + result );
    return result;
  }

  /**
   * @see javax.management.NotificationBroadcasterSupport#getNotificationInfo()
   */
  @Override
  public MBeanNotificationInfo[] getNotificationInfo()
  {
    String[] types = new String[] {
      AttributeChangeNotification.ATTRIBUTE_CHANGE
    };
    String name = AttributeChangeNotification.class.getName();
    String description = "An attribute of this MBean has changed";
    MBeanNotificationInfo info = new MBeanNotificationInfo( types, name, description );
    return new MBeanNotificationInfo[] {
      info
    };
  }

  /**
   * <p>
   * This method is used for formating the date.
   * </p>
   * @param dateInString
   * @return
   */
  private static String formatDate( String dateInString )
  {
    String finalDateString = null;

    if ( null != dateInString ) {
      dateInString = dateInString.trim();
      String[] dateArr = dateInString.split( ":" );
      LOGGER.info( "Date Array length is --->" + dateArr.length );

      String dateString = null;
      String minuteString = null;
      String secondString = null;

      if ( null != dateArr && dateArr.length == 3 ) {
        dateString = dateArr[0];
        minuteString = dateArr[1];
        secondString = dateArr[2];
        finalDateString = dateString + minuteString + secondString;
      }

      if ( null != dateArr && dateArr.length == 2 ) {
        dateString = dateArr[0];
        minuteString = dateArr[1];
        finalDateString = dateString + minuteString + "00";

      }
      if ( null != dateArr && dateArr.length == 1 ) {
        dateString = dateArr[0];
        if ( null != dateString && dateString.contains( " " ) ) {
          finalDateString = dateString + "00" + "00";
        } else {
          finalDateString = dateString + " 00" + "00" + "00";
        }
      }
    }
    LOGGER.info( "Finally Date String is ----->" + finalDateString );
    return finalDateString;
  }
}
