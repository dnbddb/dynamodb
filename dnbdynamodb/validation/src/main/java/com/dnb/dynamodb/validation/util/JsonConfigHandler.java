package com.dnb.dynamodb.validation.util;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.dnb.dynamodb.validation.pojo.AdminDetails;
import com.dnb.dynamodb.validation.pojo.AppEnvDetails;
import com.dnb.dynamodb.validation.pojo.DynamoDBConfigInfo;
import com.dnb.dynamodb.validation.pojo.ProxyDetails;
import com.dnb.dynamodb.validation.pojo.TableCounterDetail;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * <p>
 * Config Handler class which loads all application configurations defined in config.json it loads
 * <p>
 * > counter start value which is used to set record_no column initial counter value in dynamoDB tables</>
 * <p>
 * > environment (i.e DEV,QA,PROD) details where flume is running.</>
 * </p>
 * @author IMPETUS
 * @since 17-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class JsonConfigHandler
{
  private static DynamoDBConfigInfo parConfig;

   private static final String CONFIG_FILE = "cfg/config.json";

  /**
   * Loads the configuration from JSON config file.
   * @return the status 0=Success, 1=Failed
   */

  public static int loadConfig()
  {
    int exitCode = 0;
    ObjectMapper mapper = new ObjectMapper();
    try {
      parConfig = mapper.readValue( new File( CONFIG_FILE ), DynamoDBConfigInfo.class );
      System.out.println("Configuration Loaded successfully." );
    } catch( JsonParseException e ) {
      exitCode = 1;
       System.err.println( "Error parsing the JSON config file \"" + CONFIG_FILE + "\". Error details :: " + e );
    } catch( JsonMappingException e ) {
      exitCode = 1;
       System.err.println( "Error parsing the JSON config file \"" + CONFIG_FILE + "\". Error details :: " + e );
    } catch( IOException e ) {
      exitCode = 1;
       System.err.println( "Error loading JSON config file \"" + CONFIG_FILE + "\". Error details :: " + e );
    }

    return exitCode;
  }

  /**
   * This method gets the env details like DEV, QA, PROD etc..
   * @return environment details
   */
  public static AppEnvDetails getAppEnvDetails()
  {

    return parConfig.getEnvDetails();
  }

  /**
   * This method gets the proxy setting defined in cfg/config.json
   * @return proxy details
   */
  public static ProxyDetails getProxyDetails()
  {

    return parConfig.getProxyDetails();
  }
  
  /**
   * This method accesses the "counterStart" attribute for a particular Table Name.
   * @param tableName
   *          name of table
   * @return _ the counter start value.
   */
  public static String getCounterDetails( String tableName )
  {
    String counter = "";
    TableCounterDetail tblCfgInfo = getTableNameInfo( tableName );
    if ( null != tblCfgInfo ) {
      counter = tblCfgInfo.getCounterStart();
    }
    return counter;
  }
  /**
   * This method provides a way to access all available table details by its name.
   * @param tableName
   *          name of table
   * @return Table details
   */
  public static TableCounterDetail getTableNameInfo( String tableName )
  {

    TableCounterDetail tableInfo = null;
    Map<String, TableCounterDetail> tableMap = parConfig.getTableName();

    if ( StringUtils.isNotBlank( tableName ) && null != tableMap && !tableMap.isEmpty() ) {
      tableInfo = tableMap.get( tableName );
    }
    return tableInfo;
  }
  /**
   * <p>
   * All env details related to Audit component
   * </p>
   * @return AdminDetails details
   */
  public static AdminDetails getAdminDetails()
  {

    return parConfig.getAdminDetails();
  }
}
