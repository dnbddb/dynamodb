package com.dnb.dynamodb.validation.pojo;

import java.util.Map;

/**
 * <p>
 * A POJO class to hold the configurations as described in JSON (config.json) file. config.json available in cfg/ folder.
 * </p>
 * @author IMPETUS
 * @since 17-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class DynamoDBConfigInfo
{
  private Map<String, TableCounterDetail> tableName;
  private AppEnvDetails envDetails;
  private ProxyDetails proxyDetails;
  private AdminDetails adminDetails;

  /**
   * <p>
   * See {@link #settableName(Map<String,TableCounterDetail>)}
   * </p>
   * @return Returns the tableName.
   */
  public Map<String, TableCounterDetail> getTableName()
  {
    return tableName;
  }

  /**
   * <p>
   * Set the value of <code>tableName</code>.
   * </p>
   * @param tableName
   *          The tableName to set.
   */
  public void setTableName( Map<String, TableCounterDetail> tableName )
  {
    this.tableName = tableName;
  }
  
  /**
   * <p>
   * See {@link #setenvDetails(AppEnvDetails)}
   * </p>
   * @return Returns the envDetails.
   */
  public AppEnvDetails getEnvDetails()
  {
    return envDetails;
  }

  /**
   * <p>
   * Set the value of <code>envDetails</code>.
   * </p>
   * @param envDetails
   *          The envDetails to set.
   */
  public void setEnvDetails( AppEnvDetails envDetails )
  {
    this.envDetails = envDetails;
  }

  /**
   * <p>
   * See {@link #setproxyDetails(ProxyDetails)}
   * </p>
   * @return Returns the proxyDetails.
   */
  public ProxyDetails getProxyDetails()
  {
    return proxyDetails;
  }

  /**
   * <p>
   * Set the value of <code>proxyDetails</code>.
   * </p>
   * @param proxyDetails
   *          The proxyDetails to set.
   */
  public void setProxyDetails( ProxyDetails proxyDetails )
  {
    this.proxyDetails = proxyDetails;
  }

  /**
   * <p>
   * See {@link #setadminDetails(AdminDetails)}
   * </p>
   * @return Returns the adminDetails.
   */
  public AdminDetails getAdminDetails()
  {
    return adminDetails;
  }

  /**
   * <p>
   * Set the value of <code>adminDetails</code>.
   * </p>
   * @param adminDetails The adminDetails to set.
   */
  public void setAdminDetails( AdminDetails adminDetails )
  {
    this.adminDetails = adminDetails;
  }

}
