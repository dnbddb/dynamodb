package com.dnb.dynamodb.validation.scanner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonClientException;
import com.amazonaws.services.dynamodbv2.document.BatchWriteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.ItemCollection;
import com.amazonaws.services.dynamodbv2.document.ScanOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import com.dnb.dynamodb.validation.dataobject.FeedAudit;
import com.dnb.dynamodb.validation.util.JsonConfigHandler;

public class DynamoDBTableScan
{
  private static Logger logger = LoggerFactory.getLogger( DynamoDBTableScan.class );

  private static final String FILE_NAME = "FILE_NAME";
  private String auditTable = "DNB_CIRRUS_FEED_AUDIT";

  private String processed_file;
  private String processed_date;
  private long success_count;
  private long error_count;
  private long total_count;
  private String envDetail;
  private String auditTableName;

  private String[] headerArr = getheaderArray();
  private TableWriteItems tableWriteItems;
  // unprocessed items handling
  private BatchWriteItemOutcome outcome;
  private Collection<Item> auditReadingsToPut = new ArrayList<Item>();


  public void auditTable( DynamoDB dynamo, String tableName, String errtableName, String processDate )
  {
    try {
      envDetail = JsonConfigHandler.getAppEnvDetails().getEnv().trim().toUpperCase();
      if ( !( envDetail.equals( "" ) || envDetail.equals( null ) ) ) {
        auditTableName = auditTable.concat( "_" + envDetail );
      } else {
        auditTableName = auditTable;
      }

      tableWriteItems = new TableWriteItems( auditTableName );

      Map<String, Object> expressionAttributeValues = new HashMap<String, Object>();
      expressionAttributeValues.put( ":proc_date", processDate );

      Table table = dynamo.getTable( tableName );
      Table errtable = dynamo.getTable( errtableName );

      logger.info( "Scanning Table > " + tableName + " , Error Table > " + errtableName );

      Map<String, List<Item>> table_map = new HashMap<String, List<Item>>();
      Map<String, List<Item>> errtable_map = new HashMap<String, List<Item>>();

      ItemCollection<ScanOutcome> items = table.scan( "DATE_CREATED = :proc_date", "DATE_CREATED, FILE_NAME", null, expressionAttributeValues );
      ItemCollection<ScanOutcome> erritems = errtable.scan( "DATE_CREATED = :proc_date", "DATE_CREATED, FILE_NAME", null, expressionAttributeValues );

      for( Item tableItem : items ) {
        String key = tableItem.getString( FILE_NAME );

        if ( table_map.containsKey( key ) ) {
          List<Item> list = table_map.get( key );
          list.add( tableItem );
        } else {
          List<Item> list = new ArrayList<Item>();
          list.add( tableItem );
          table_map.put( key, list );
        }
      }

      for( Item errtableItem : erritems ) {
        String key = errtableItem.getString( FILE_NAME );

        if ( errtable_map.containsKey( key ) ) {
          List<Item> list = errtable_map.get( key );
          list.add( errtableItem );
        } else {
          List<Item> list = new ArrayList<Item>();
          list.add( errtableItem );
          errtable_map.put( key, list );
        }
      }

      Set<String> tablekeySet = table_map.keySet();

      for( String key : tablekeySet ) {
        Item validItemObj = new Item();
        List<String> row = new ArrayList<String>();
        processed_file = key;
        processed_date = processDate;
        success_count = table_map.get( key ).size();

        if ( errtable_map.containsKey( key ) ) {
          error_count = errtable_map.get( key ).size();
        } else {
          error_count = 0;
        }
        if ( AuditGenerator.input_file_map.containsKey( key ) ) {
          total_count = Long.parseLong( AuditGenerator.input_file_map.get( key ) );
        } else {
          total_count = 0;
        }
        // total_count = success_count + error_count;
        row.add( processed_file );
        row.add( processed_date );
        row.add( String.valueOf( success_count ) );
        row.add( String.valueOf( error_count ) );
        row.add( String.valueOf( total_count ) );
        for( int k = 0; k < headerArr.length; k++ ) {
          validItemObj.withString( headerArr[k], row.get( k ) );
        }
        auditReadingsToPut.add( validItemObj );

        logger.info( "TableName > " + tableName + ", " + "PROCESSED_FILE > " + processed_file + ", " + "PROCESSED_DATE > " + processed_date + ", "
          + "SUCCESS_COUNT > " + success_count + ", " + "ERROR_COUNT > " + error_count + ", " + "TOTAL_COUNT > " + total_count );
      }

      Collection<Item> localitemsToPut = new ArrayList<Item>();
      localitemsToPut.addAll( auditReadingsToPut );
      tableWriteItems.withItemsToPut( localitemsToPut );

      if ( localitemsToPut.size() > 0 ) {
        outcome = dynamo.batchWriteItem( tableWriteItems );
        do {
          // Check for unprocessed keys which could happen if you exceed provisioned throughput
          Map<String, List<WriteRequest>> unprocessedItems = outcome.getUnprocessedItems();
          if ( outcome.getUnprocessedItems().size() == 0 ) {
            logger.info( "No unprocessed items found" );
          } else {
            logger.info( "Retrieving the unprocessed items" );
            outcome = dynamo.batchWriteItemUnprocessed( unprocessedItems );
          }
        } while ( outcome.getUnprocessedItems().size() > 0 );
      }
      logger.info( "Inserted statistics for " + tableName + " successfully" );
      
      if ( auditReadingsToPut.size() > 0 )
        auditReadingsToPut.clear();
    } catch( AmazonClientException | IllegalArgumentException | ClassCastException | UnsupportedOperationException e ) {
      logger.error( "Unable to load audit details" + e.getMessage());
    }
  }

  private String[] getheaderArray()
  {
    return FeedAudit.HEADER.split( "," );
  }

}
