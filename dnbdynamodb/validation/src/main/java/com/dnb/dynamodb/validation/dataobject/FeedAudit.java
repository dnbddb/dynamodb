package com.dnb.dynamodb.validation.dataobject;

/**
 * <p>
 * Represents DataObject of audit table "DNB_CIRRUS_FEED_AUDIT" in DynamoDB
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public interface FeedAudit
{
  String PROCESSED_FILE = "PROCESSED_FILE";
  String PROCESSED_DATE = "PROCESSED_DATE";
  String SUCCESS_COUNT = "SUCCESS_COUNT";
  String ERROR_COUNT = "ERROR_COUNT";
  String TOTAL_COUNT = "TOTAL_COUNT";


  String HEADER = PROCESSED_FILE + "," + PROCESSED_DATE + "," + SUCCESS_COUNT + "," + ERROR_COUNT + "," + TOTAL_COUNT;
}
