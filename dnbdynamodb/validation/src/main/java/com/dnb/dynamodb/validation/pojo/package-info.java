/**
 * <p>
 * Package having pojo classes which are holding the configurations as described in JSON (config.json) file. config.json available in cfg/ folder.
 * </p>
 * @author IMPETUS
 * @since 17-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
package com.dnb.dynamodb.validation.pojo;