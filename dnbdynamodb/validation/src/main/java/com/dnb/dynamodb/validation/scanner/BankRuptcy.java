package com.dnb.dynamodb.validation.scanner;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.dnb.dynamodb.validation.connecter.DynamoDBConnector;
import com.dnb.dynamodb.validation.util.JsonConfigHandler;
/**
 * <p>
 * Scan DNB_CIRRUS_BANKRUPTCY_FEED table in DyanamoDB and generate audit reports corresponding to this table.
 * </p>
 * @author IMPETUS
 * @since 22-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in> 
 *
 */
public class BankRuptcy extends DynamoDBTableScan implements Runnable
{
  private DynamoDB client = DynamoDBConnector.getClient();
  private String table = "DNB_CIRRUS_BANKRUPTCY_FEED";
  private String errtable = "DNB_CIRRUS_BANKRUPTCY_FEED_ERROR";
  private String processDate;
  private String envDetail;
  private String tableName;
  private String errtableName;

  public BankRuptcy( String processDate )
  {
    super();
    this.processDate = processDate;
  }

  @Override
  public void run()
  {
    envDetail = JsonConfigHandler.getAppEnvDetails().getEnv().trim().toUpperCase();
    if ( !( envDetail.equals( "" ) || envDetail.equals( null ) ) ) {
      tableName = table.concat( "_" + envDetail );
      errtableName = errtable.concat( "_" + envDetail );
    } else {
      tableName = table;
      errtableName = errtable;
    }
    this.auditTable( client, tableName, errtableName, processDate );
  }

}
