/**
 * <p>
 * Package having classes which scan various tables from dynamoDB and feed files from feed location to generate audit reports and finally load these
 * generated statistics in audit tables.
 * </p>
 * @author IMPETUS
 * @since 17-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
package com.dnb.dynamodb.validation.scanner;