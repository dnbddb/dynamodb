package com.dnb.dynamodb.validation.pojo;

/**
 * <p>
 * A POJO class which holds the config information of a particular table.
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class TableCounterDetail
{
  private String counterStart;

  /**
   * <p>
   * See {@link #setcounterStart(String)}
   * </p>
   * @return Returns the counterStart.
   */
  public String getCounterStart()
  {
    return counterStart;
  }

  /**
   * <p>
   * Set the value of <code>counterStart</code>.
   * </p>
   * @param counterStart
   *          counterStart to set.
   */
  public void setCounterStart( String counterStart )
  {
    this.counterStart = counterStart;
  }

}
