package com.dnb.dynamodb.validation.scanner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.services.cloudfront.model.InvalidArgumentException;
import com.dnb.dynamodb.validation.util.JsonConfigHandler;

/**
 * <p>
 * This is the main class where application get started and pickup supplied arguments. Supplied arguments are: 1. Input File Path: File path of config
 * file where mappings between filename and total records information kept. 2. Process Date: Date for which audit statistics needs to be generated.
 * </p>
 * @author IMPETUS
 * @since 22-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class AuditGenerator
{
  private static Logger logger = LoggerFactory.getLogger( AuditGenerator.class );
  public static Map<String, String> input_file_map = new HashMap<String, String>();

  /**
   * <p>
   * Main mentod: entry point for application. Expecting two input arguments:
   * 1. Input File Path
   * 2. Process Date in YYYYMMDD format
   * </p>
   * @param args
   * @throws IOException
   */
  public static void main( String[] args ) throws InvalidArgumentException, FileNotFoundException, IOException
  {
    int configStatus = JsonConfigHandler.loadConfig();
    if ( 0 != configStatus ) {
      System.exit( configStatus );
    }

    if ( args.length < 2 || !AuditGenerator.validateInputArgs( args ) ) {
      System.err.println( "Please check the supplied arguments, either they are not in correct format or not supplied at all." );
      System.err.println( "USAGE: AuditGenerator <<1.Input File Path >e.g.:/home/file.txt> <<2.Process Date>>" );
      return;
    }

    org.apache.log4j.PropertyConfigurator.configure( "cfg/audit_log4j.properties" );
    
    AuditGenerator.loadFile( args[0] );
    AuditGenerator.generateAuditReports( args[1] );
    logger.info( "Audit Report process for process date '" + args[1] + "' completed." );
  }

  /**
   * This method validates the input parameters given to AuditGenerator.
   * @param args : array of input parameters.
   * @return the boolean validation result.
   */
  private static boolean validateInputArgs( String[] args )
  {
    boolean result = false;
    if ( ( StringUtils.isNotBlank( args[0] ) && new File( args[0] ).exists() )
      && ( StringUtils.isNotBlank( args[1] ) && StringUtils.isNumeric( args[1] ) ) )
      result = true;
    return result;
  }

  /**
   * <p>
   * This method load configurations from input file given to AuditGenerator.
   * </p>
   * @param filePath
   * @throws IOException
   */
  private static void loadFile( String filePath ) throws InvalidArgumentException, FileNotFoundException, IOException
  {
    Path fFilePath = Paths.get( filePath );
    Charset ENCODING = StandardCharsets.UTF_8;

    try (Scanner fileScanner = new Scanner( fFilePath, ENCODING.name() )) {
      while ( fileScanner.hasNextLine() ) {
        String aLine = fileScanner.nextLine();
        Scanner lineScanner = new Scanner( aLine );
        lineScanner.useDelimiter( "=" );
        if ( lineScanner.hasNext() ) {
          // assumes the line has a certain structure
          String key = lineScanner.next();
          String value = lineScanner.next();
          input_file_map.put( key, value );
          logger.info( "Input File > key is : " + key.trim() + ", and Value is : " + value.trim() );
        } else {
          logger.error( "Empty or invalid line. Unable to process." );
        }
        lineScanner.close();
      }
    }
  }

  /**
   * <p>
   * This method scan various tables from dynamoDB and feed files from input file location to generate audit reports and finally load these generated
   * statistics in audit tables.
   * </p>
   * @param procDate
   */
  private static void generateAuditReports( String procDate )
  {
    ExecutorService exec = Executors.newCachedThreadPool();

    exec.submit( new BankRuptcy( procDate ) );
    exec.submit( new CssFssFeed( procDate ) );
    exec.submit( new CssFssTrend( procDate ) );
    exec.submit( new Financials( procDate ) );
    exec.submit( new GlobalFeed( procDate ) );
    exec.submit( new IdentityFeed( procDate ) );
    exec.submit( new LinkageFeed( procDate ) );
    exec.submit( new PaydexRatingFeed( procDate ) );
    exec.submit( new PaydexTrend( procDate ) );
    exec.submit( new SubjectFeed( procDate ) );

    exec.shutdown();
    try {
      exec.awaitTermination( 25, TimeUnit.MINUTES );
    } catch( InterruptedException | RuntimeException e ) {
      logger.error( "Exception occuured while genetaing Reports" + e.getMessage());
    }
  }
}
