/**
 * <p>
 * This Package has utility class which is used to connect Amazon DynamoDB. It Read Secret Key and Value to get connection with DynamoDB from
 * cfg/DbCredentials.properties. These credentials are used to securely sign requests to AWS services.
 * </p>
 * @author IMPETUS
 * @since 17-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
package com.dnb.dynamodb.validation.connecter;