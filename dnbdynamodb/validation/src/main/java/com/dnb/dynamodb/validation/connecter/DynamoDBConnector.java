package com.dnb.dynamodb.validation.connecter;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.dnb.dynamodb.validation.pojo.ProxyDetails;
import com.dnb.dynamodb.validation.util.JsonConfigHandler;

/**
 * <p>
 * The DB connector class which create connection with DynamoDB. This is a factory class.
 * </p>
 * @author IMPETUS
 * @since 17-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class DynamoDBConnector
{
  private static Logger logger = LoggerFactory.getLogger( DynamoDBConnector.class );

  private static String accessFile = "cfg/dbCredentials.properties";
  
  /**
   * Read Secret Key and Value to get connection with DynamoDB. These credentials are used to securely sign requests to AWS services
   * @return AWS access key ID and secret access key
   */
  public static AWSCredentials awsTestCredentials()
  {
    try {
      return new PropertiesCredentials( new File( accessFile ) );
    } catch( IOException | IllegalArgumentException e ) {
      logger.error( "Can't connect to DynamoDB using given access key", e );
      throw new RuntimeException( e );
    }
  }

  /**
   * Create Connection to Amazon DyanoDB
   * @return connection instance of DyanamoDB
   */
  public static DynamoDB getClient()
  {
    ProxyDetails proxyDetail = JsonConfigHandler.getProxyDetails();
    AmazonDynamoDBClient client = null;
    try {

      if (null != proxyDetail.getProxyFlag() && proxyDetail.getProxyFlag().equalsIgnoreCase( "false" ) ) {
        client = new AmazonDynamoDBClient( awsTestCredentials() );
      } else {
        Properties props = System.getProperties();
        props.setProperty( "http.proxyHost", proxyDetail.getProxyHost().trim() );
        props.setProperty( "https.proxyPort", proxyDetail.getProxyPort().trim() );
        ClientConfiguration cconfig = new ClientConfiguration();
        cconfig.setProxyHost( props.getProperty( "http.proxyHost" ) );
        cconfig.setProxyPort( Integer.parseInt( props.getProperty( "https.proxyPort" ) ) );
        client = new AmazonDynamoDBClient( awsTestCredentials(), cconfig );
      }
      if(null != proxyDetail.getRegion())
      client.setRegion(Regions.fromName( proxyDetail.getRegion().trim()));
      else
        logger.info( "Region Details are not provided." );
      
      return new DynamoDB( client );
    } catch( Exception e ) {
      logger.error( "Can't connect to DynamoDB", e );
      return null;
    }
  }
}
