/**
 * <p>
 * Config Handler package which loads all application configurations defined in config.json it loads
 * <p>
 * > counter start value which is used to set record_no column initial counter value in dynamoDB tables</>
 * <p>
 * > environment (i.e DEV,QA,PROD) details where flume is running.</>
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
package com.dnb.dynamodb.ingestion.util;