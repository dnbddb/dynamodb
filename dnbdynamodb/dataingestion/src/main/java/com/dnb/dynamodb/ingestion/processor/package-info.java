/**
 * <p>
 * Package holding classes which process a feed and apply all required business rules so that it can be inserted in corresponding DynamoDB table.
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
package com.dnb.dynamodb.ingestion.processor;