/**
 * <p>
 * Package having custom Sinks for Apache Flume which Loads Data from Channel and save it in corresponding table in DyanamoDB Currently for each table
 * where data load operations are required having separate custom sink.
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
package com.dnb.dynamodb.ingestion.sink;