package com.dnb.dynamodb.ingestion.dataobject;

/**
 * <p>
 * Represents DataObject of Table DNB_CIRRUS_IDENTITY_FEED in DynamoDB
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public interface IdentityFeed
{
  String DUNS_NO = "DUNS_NO";
  String DATE_CREATED = "DATE_CREATED";
  String FILE_NAME = "FILE_NAME";
  String RECORD_NO = "RECORD_NO";
  String CITY = "CITY";
  String COUNTRY = "COUNTRY";
  String NAME = "NAME";
  String TRADE_STYLE = "TRADE_STYLE";
  String TRADE_STYLE2 = "TRADE_STYLE2";
  String TRADE_STYLE3 = "TRADE_STYLE3";
  String STREET1 = "STREET1";
  String STREET2 = "STREET2";
  String ZIP = "ZIP";
  String STATE = "STATE";
  String PHONE_NO = "PHONE_NO";
  String CEO_NAME = "CEO_NAME";
  String LEGAL_STRUCTURE = "LEGAL_STRUCTURE";
  String LEGAL_FORM_CLASS_CODE = "LEGAL_FORM_CLASS_CODE";
  String YEAR_STARTED = "YEAR_STARTED";
  String INC_YEAR = "INC_YEAR";
  String MOVED_IND = "MOVED_IND";

  String HEADER = RECORD_NO + "," + DUNS_NO + "," + DATE_CREATED + "," + FILE_NAME + "," + CITY + "," + COUNTRY + "," + NAME + "," + TRADE_STYLE
    + "," + TRADE_STYLE2 + "," + TRADE_STYLE3 + "," + STREET1 + "," + STREET2 + "," + ZIP + "," + STATE + "," + PHONE_NO + "," + CEO_NAME + ","
    + LEGAL_STRUCTURE + "," + LEGAL_FORM_CLASS_CODE + "," + YEAR_STARTED + "," + INC_YEAR + "," + MOVED_IND;

}
