package com.dnb.dynamodb.ingestion.sink;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import org.apache.flume.Channel;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.EventDeliveryException;
import org.apache.flume.Transaction;
import org.apache.flume.conf.Configurable;
import org.apache.flume.sink.AbstractSink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.document.BatchWriteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import com.dnb.dynamodb.ingestion.connecter.DynamoDBConnector;
import com.dnb.dynamodb.ingestion.dataobject.PaydexTrend;
import com.dnb.dynamodb.ingestion.processor.FeedProcessor;
import com.dnb.dynamodb.ingestion.util.SinkConfigHandler;
import com.google.common.base.Throwables;

/**
 * <p>
 * A custom Sink for Apache Flume which Loads Data from Channel and save it in DyanamoDB
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class PaydexTrendSink
  extends AbstractSink
  implements Configurable
{
  private static Logger logger = LoggerFactory.getLogger( PaydexTrendSink.class );

  // configurable items
  public static final String NAME_PREFIX = "PaydexTrendSink_";
  public static final String BATCH_SIZE = "batch";
  public static final String TABLE_NAME = "table";
  public static final String ERR_TABLE_NAME = "errtable";

  // Default Values for configurable items
  private static AtomicLong DEFAULT_COUNTER_START = new AtomicLong();
  public static final int DEFAULT_BATCH = 25;
  public static final String DEFAULT_TABLENAME = "DNB_CIRRUS_PAYDEX_TREND";
  public static final String DEFAULT_ERRTABLENAME = "DNB_CIRRUS_PAYDEX_TREND_ERROR";

  private int batchSize;
  private static String table;
  private static String errortable;
  private DynamoDB dynamo;

  /**
   * Load all configured values from flume conf file
   */
  @Override
  public void configure( Context context )
  {
    setName( NAME_PREFIX + DEFAULT_COUNTER_START.getAndIncrement() );
    batchSize = context.getInteger( BATCH_SIZE, DEFAULT_BATCH );
    table = context.getString( TABLE_NAME, DEFAULT_TABLENAME );
    errortable = context.getString( ERR_TABLE_NAME, DEFAULT_ERRTABLENAME );
    logger.info( "DynamoSink {} context { table: {} }", new Object[] {
      getName(), table
    } );

  }

  // Amazon DB variables
  private TableWriteItems tableWriteItems;
  private TableWriteItems errtableWriteItems;
  private AtomicLong tbl_counter = new AtomicLong();
  private AtomicLong errtbl_counter = new AtomicLong();
  private String envDetail = null;
  private String flumeEventDir = null;
  private String flumeLogFileName = null;
  private File eventLogFile = null;
  private boolean isFileExists = false;
  SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

  // unprocessed items handling
  private BatchWriteItemOutcome outcome;

  /**
   * Start Flume
   */
  @Override
  public void start()
  {
    // Load properties from JSON configuration
    int configStatus = SinkConfigHandler.loadConfig();
    if ( 0 != configStatus ) {
      System.exit( configStatus );
    }

    try {
      tbl_counter.set( Long.parseLong( SinkConfigHandler.getCounterDetails( table ) ) );
    } catch( NumberFormatException e ) {
      tbl_counter.set( Long.parseLong( "1" ) );
    }
    try {
      errtbl_counter.set( Long.parseLong( SinkConfigHandler.getCounterDetails( errortable ) ) );
    } catch( NumberFormatException e ) {
      errtbl_counter.set( Long.parseLong( "1" ) );
    }

    envDetail = SinkConfigHandler.getAppEnvDetails().getEnv().trim().toUpperCase();

//    String strDate = dateFormat.format(java.util.Calendar.getInstance().getTime());
    flumeEventDir = SinkConfigHandler.getAppEnvDetails().getFlumeEventlogs();
    
    if ( ( flumeEventDir.equals( "" ) || flumeEventDir.equals( null ) ) ) {
      flumeEventDir = "/tmp";
    }
//    flumeLogFileName = flumeEventDir.concat( "/PaydexTrendSink_" ) + strDate + ".log";
//    eventLogFile = new File(flumeLogFileName);
//    try {
//      isFileExists = eventLogFile.createNewFile();
//    } catch( IOException e ) {
//      e.printStackTrace();
//    }
    
    if ( !( envDetail.equals( "" ) || envDetail.equals( null ) ) ) {
      tableWriteItems = new TableWriteItems( table.concat( "_" + envDetail ) );
      errtableWriteItems = new TableWriteItems( errortable.concat( "_" + envDetail ) );
    } else {
      tableWriteItems = new TableWriteItems( table );
      errtableWriteItems = new TableWriteItems( errortable );
    }

    // Initialize the connection to the external repository (e.g. DynamoDB) that
    // this Sink will forward Events to ..
    dynamo = DynamoDBConnector.getConnection();
    logger.info( "Starting {}...", getName() );
    super.start();
    logger.info( "Started {}.", getName() );
  }

  /**
   * Process an events/records which flume picks up from channel.
   */
  @Override
  public Status process()
    throws EventDeliveryException
  {
    logger.debug( "{} start to process event", getName() );
    Status status = Status.READY;
    status = parseEvents();
    logger.debug( "{} processed event", getName() );
    return status;
  }

  /**
   * <p>
   * parse events/records picked up from channel and start putting /insert these records in dynamoDB table.
   * </p>
   * @return
   * @throws EventDeliveryException
   */
  private Status parseEvents()
    throws EventDeliveryException
  {
    Status status = Status.READY;
    Channel channel = getChannel();
    Collection<Item> validItemsToPut = new ArrayList<Item>();
    Collection<Item> errorItemsToPut = new ArrayList<Item>();
    Transaction tx = null;
    try {
      tx = channel.getTransaction();
      tx.begin();
      for( int i = 0; i < batchSize; i++ ) {
        Event event = channel.take();
        if ( event == null ) {
          status = Status.BACKOFF;
          break;
        } else {
          processEvent( event, validItemsToPut, errorItemsToPut );
        }
      }

      tableWriteItems.withItemsToPut( validItemsToPut );

      if ( validItemsToPut.size() > 0 ) {
        outcome = dynamo.batchWriteItem( tableWriteItems );
        do {
          // Check for unprocessed keys which could happen if you
          // exceed provisioned throughput
          Map<String, List<WriteRequest>> unprocessedItems = outcome.getUnprocessedItems();
          if ( outcome.getUnprocessedItems().size() == 0 ) {
            logger.info( "No unprocessed items found" );
          } else {
            logger.info( "Retrieving the unprocessed items" );
            outcome = dynamo.batchWriteItemUnprocessed( unprocessedItems );
          }
        } while ( outcome.getUnprocessedItems().size() > 0 );
      }
      if ( validItemsToPut.size() > 0 )
        validItemsToPut.clear();

      errtableWriteItems.withItemsToPut( errorItemsToPut );
      if ( errorItemsToPut.size() > 0 )
        dynamo.batchWriteItem( errtableWriteItems );
      if ( errorItemsToPut.size() > 0 )
        errorItemsToPut.clear();

      tx.commit();
    } catch( AmazonServiceException e ) {
      logger.error( "Amazon Service Exception, current batch is not processed" );
      String strDate = dateFormat.format( java.util.Calendar.getInstance().getTime() );

      flumeLogFileName = flumeEventDir.concat( "/PaydexTrendSink_" ) + strDate + ".log";
      eventLogFile = new File( flumeLogFileName );
      try {
        isFileExists = eventLogFile.createNewFile();
      } catch( IOException exc ) {
        exc.printStackTrace();
      }

      if ( isFileExists ) {
        try {
          FileWriter fw = new FileWriter( eventLogFile.getAbsoluteFile() );
          BufferedWriter bw = new BufferedWriter( fw );
          String date = "Current Batch Items which were not process on : " + java.util.Calendar.getInstance().getTime().toString() + " are:";
          bw.newLine();
          bw.append( date );
          bw.newLine();
          
          for( Item baditem : validItemsToPut )
          {
            String dun_sno = baditem.getString( "DUNS_NO" );
            String date_created = baditem.getString( "DATE_CREATED" );
            String file_name = baditem.getString( "FILE_NAME" );
            
            bw.append( "DUNS_NO = ");
            bw.append(dun_sno);
            bw.append(" , ");
            bw.append("DATE_CREATED = ");
            bw.append(date_created);
            bw.append(" , ");
            bw.append("FILE_NAME");
            bw.append(file_name);
            bw.newLine();
          }
          bw.append( errorItemsToPut.toString() );
          bw.newLine();
          bw.append( "---------------------------------------------------------------------------------------------------" );
          bw.newLine();
          bw.close();
          logger.error( "Bad events are written to log file" );
        } catch( IOException ex ) {
          ex.printStackTrace();
        }
      } else {
        try {
          FileWriter fw = new FileWriter( eventLogFile.getAbsoluteFile() );
          BufferedWriter bw = new BufferedWriter( fw );
          String date = "Current Batch Items which were not process on : " + java.util.Calendar.getInstance().getTime().toString() + " are:";
          bw.newLine();
          bw.append( date );
          bw.newLine();
          
          for( Item baditem : validItemsToPut )
          {
            String dun_sno = baditem.getString( "DUNS_NO" );
            String date_created = baditem.getString( "DATE_CREATED" );
            String file_name = baditem.getString( "FILE_NAME" );
            
            bw.append( "DUNS_NO = ");
            bw.append(dun_sno);
            bw.append(" , ");
            bw.append("DATE_CREATED = ");
            bw.append(date_created);
            bw.append(" , ");
            bw.append("FILE_NAME = ");
            bw.append(file_name);
            bw.newLine();
          }
          bw.append( errorItemsToPut.toString() );
          bw.newLine();
          bw.append( "---------------------------------------------------------------------------------------------------" );
          bw.close();
          logger.error( "Bad events are written to log file" );
        } catch( IOException ex ) {
          ex.printStackTrace();
        }
      }
      tx.commit();
    } catch( Throwable ex ) {
      logger.error( "can't process events, Throwable Exception!" + ex.getMessage() );
      tx.rollback();// rollback to drop bad event, otherwise it will enter dead loop.
      if ( errorItemsToPut.size() > 0 )
        errorItemsToPut.clear();

      if ( validItemsToPut.size() > 0 )
        validItemsToPut.clear();

      if ( ex instanceof Error || ex instanceof RuntimeException ) {
        logger.error( "Failed to commit transaction. Transaction rolled back.", ex );
        Throwables.propagate( ex );
      } else {
        logger.error( "Failed to commit transaction. Transaction rolled back.", ex );
        throw new EventDeliveryException( "Failed to commit transaction. Transaction rolled back.", ex );
      }

    } finally {
      tx.close();
    }

    return status;
  }

  /**
   * <p>
   * pick up single record from channel and validate all columns values before it can be ready for insertion in dynamoDB table.
   * </p>
   * @param event
   * @throws IOException
   */
  private void processEvent( Event event, Collection<Item> validItemsToPut, Collection<Item> errorItemsToPut )
  {
    Item validItemObj = new Item();
    Item errItemObj = new Item();
    String val = null;
    String[] headerArr = getheaderArray();

    // String line = new String( event.getBody(), Charsets.UTF_16 );
    String line = new String( event.getBody() );
    // String[] columnVal = line.split("\\s*,\\s*");
    String[] columnVal = line.trim().split( "\\*" );
    List<String> row = new ArrayList<String>( Arrays.asList( columnVal ) );

    if ( row.contains( PaydexTrend.DUNS_NO ) || row.contains( PaydexTrend.DATE_CREATED ) )
      return;

    if ( row.get( 0 ).equals( "" ) ) {
      long errtbl_count = errtbl_counter.getAndIncrement();
      row.add( 0, Long.toString( errtbl_count ) );
      for( int k = 0; k < row.size(); k++ ) {
        val = FeedProcessor.process( headerArr[k], ( row.get( k ) ).trim() );
        if ( headerArr[k].equalsIgnoreCase( PaydexTrend.RECORD_NO ) ) {
          errItemObj.withLong( headerArr[k], Long.parseLong( val ) );
        } else {
          // errItemObj.withString( headerArr[k], ( row.get( k ).equals( "" ) ? " " : val ) );
          errItemObj.with( headerArr[k], ( row.get( k ).equals( "" ) ? " " : val ) );
        }
      }
      errorItemsToPut.add( errItemObj );
    } else {
      long tbl_count = tbl_counter.getAndIncrement();
      row.add( 0, Long.toString( tbl_count ) );
      for( int j = 0; j < row.size(); j++ ) {
        val = FeedProcessor.process( headerArr[j], ( row.get( j ) ).trim() );

        if ( headerArr[j].equalsIgnoreCase( PaydexTrend.RECORD_NO ) ) {
          validItemObj.withLong( headerArr[j], Long.parseLong( val ) );
        } else {
          // validItemObj.withString( headerArr[j], ( row.get( j ).equals( "" ) ? " " : val ) );
          validItemObj.with( headerArr[j], ( row.get( j ).equals( "" ) ? " " : val ) );
        }
      }
      validItemsToPut.add( validItemObj );
    }

  }

  /**
   * <p>
   * returns names of all columns having in given table.
   * </p>
   * @return
   */
  private String[] getheaderArray()
  {
    return PaydexTrend.HEADER.split( "," );
  }

  /**
   * Stop sink in case ctrl C event received to abort Flume Process.
   * @see org.apache.flume.sink.AbstractSink#stop()
   */
  @Override
  public void stop()
  {
    logger.debug( "Shutting down DynamoDB Sink..." );
    dynamo.shutdown();
    super.stop();
  }

}