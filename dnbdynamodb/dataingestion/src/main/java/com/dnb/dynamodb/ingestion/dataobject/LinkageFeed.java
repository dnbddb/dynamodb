package com.dnb.dynamodb.ingestion.dataobject;

/**
 * <p>
 * Represents DataObject of Table DNB_CIRRUS_LINKAGE_FEED in DynamoDB
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public interface LinkageFeed
{
  String DUNS_NO = "DUNS_NO";
  String DATE_CREATED = "DATE_CREATED";
  String FILE_NAME = "FILE_NAME";
  String RECORD_NO = "RECORD_NO";
  String DOM_ULTIM_DUNS_NUMBER = "DOM_ULTIM_DUNS_NUMBER";
  String DOM_ULTIM_NAME = "DOM_ULTIM_NAME";
  String DOM_ULTIM_COUNTRY_CODE = "DOM_ULTIM_COUNTRY_CODE";
  String GLOBAL_ULTIM_DUNS_NUMBER = "GLOBAL_ULTIM_DUNS_NUMBER";
  String GLOBAL_ULTIM_NAME = "GLOBAL_ULTIM_NAME";
  String GLOBAL_ULTIM_COUNTRY_CODE = "GLOBAL_ULTIM_COUNTRY_CODE";
  String HEAD_DUNS_NUMBER = "HEAD_DUNS_NUMBER";
  String HEAD_NAME = "HEAD_NAME";
  String HEADQTR_COUNTRY_NAME = "HEADQTR_COUNTRY_NAME";
  String LOCATION_TYPE = "LOCATION_TYPE";


  String HEADER = RECORD_NO + "," + DUNS_NO + "," + DATE_CREATED + "," + FILE_NAME + "," + DOM_ULTIM_DUNS_NUMBER + "," + DOM_ULTIM_NAME + ","
    + DOM_ULTIM_COUNTRY_CODE + "," + GLOBAL_ULTIM_DUNS_NUMBER + "," + GLOBAL_ULTIM_NAME + "," + GLOBAL_ULTIM_COUNTRY_CODE + "," + HEAD_DUNS_NUMBER
    + "," + HEAD_NAME + "," + HEADQTR_COUNTRY_NAME + "," + LOCATION_TYPE;
}
