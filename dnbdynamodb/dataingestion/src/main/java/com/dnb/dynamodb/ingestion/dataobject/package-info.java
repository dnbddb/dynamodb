/**
 * <p>
 * Package has classes which are representing DataObject of various tables in DynamoDB.
 * Each class resembles schema of corresponding table in DynamoDB.
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
package com.dnb.dynamodb.ingestion.dataobject;