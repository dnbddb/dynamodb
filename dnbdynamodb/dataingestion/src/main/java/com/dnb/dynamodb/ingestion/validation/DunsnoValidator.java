package com.dnb.dynamodb.ingestion.validation;

import java.io.IOException;

/**
 * <p>
 * Class to validate/transform DUNS_NO field value If DUNSNO
 * <p>
 * field having length = 9: Do nothing Simple return the value
 * </p>
 * <p>
 * field having length < 9: prepend 0 to make length = 9 and return the value
 * </p>
 * <p>
 * field having length > 9: substring 9 digits and return the value
 * </p>
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class DunsnoValidator
{
  public static String validateDunsno( String in )
    throws IOException
  {
    String input = in.replaceAll( "[^\\d]", "" );
    String result = null;
    if ( input.length() == 0 ) {
      return result;
    } else if ( input.length() == 9 ) {
      result = input;
      return result;
    } else if ( input.length() < 9 && input.length() > 0 ) {
      result = String.format( "%09d", Long.parseLong( input ) );
      return result;
    } else {
      result = input.substring( 0, 9 );
      return result;
    }
  }
}
