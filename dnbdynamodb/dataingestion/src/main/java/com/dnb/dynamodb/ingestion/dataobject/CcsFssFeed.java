package com.dnb.dynamodb.ingestion.dataobject;

/**
 * <p>
 * Represents DataObject of Table DNB_CIRRUS_CCS_FSS_FEED in DynamoDB
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public interface CcsFssFeed
{

  String DUNS_NO = "DUNS_NO";
  String DATE_CREATED = "DATE_CREATED";
  String FILE_NAME = "FILE_NAME";
  String RECORD_NO = "RECORD_NO";
  String COMMERCIAL_CREDIT_SCORE = "COMMERCIAL_CREDIT_SCORE";
  String COMM_CR_SCR_CLASS = "COMM_CR_SCR_CLASS";
  String COMM_CR_SCR_PERCENTILE = "COMM_CR_SCR_PERCENTILE";
  String FINANCIAL_STRESS_SCORE = "FINANCIAL_STRESS_SCORE";
  String FIN_STRESS_CLASS = "FIN_STRESS_CLASS";
  String FIN_STRESS_SCR_PERCENTILE = "FIN_STRESS_SCR_PERCENTILE";

  String HEADER = RECORD_NO + "," + DUNS_NO + "," + DATE_CREATED + "," + FILE_NAME + "," + COMMERCIAL_CREDIT_SCORE + "," + COMM_CR_SCR_CLASS + ","
    + COMM_CR_SCR_PERCENTILE + "," + FINANCIAL_STRESS_SCORE + "," + FIN_STRESS_CLASS + "," + FIN_STRESS_SCR_PERCENTILE;
}