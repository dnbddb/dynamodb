package com.dnb.dynamodb.ingestion.processor;


/**
 * <p>
 * Class to process a record and apply all required business rules so that it can be inserted in corresponding DynamoDB table.
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public class FeedProcessor
{
  protected static final String DUNS_NO = "DUNS_NO";

  // public static String process( String columnName, String columnValue )
  // throws IOException
  // {
  // if ( columnName.equalsIgnoreCase( DUNS_NO ) ) {
  // return DunsnoValidator.validateDunsno( columnValue );
  // } else {
  // return columnValue;
  // }
  // }

  public static String process( String columnName, String columnValue )
  {
    if ( columnName.equalsIgnoreCase( DUNS_NO ) )
      return columnValue;
    else if ( columnValue.startsWith( "+" ) )
      return ( columnValue.substring( 1 ) ).replaceFirst( "^0+(?!$)", "" );
    else if ( columnValue.startsWith( "-" ) )
      return "-" + ( columnValue.substring( 1 ) ).replaceFirst( "^0+(?!$)", "" );
    else
      return columnValue.replaceFirst( "^0+(?!$)", "" );
  }
}
