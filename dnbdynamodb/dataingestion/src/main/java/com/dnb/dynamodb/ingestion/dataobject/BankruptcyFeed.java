package com.dnb.dynamodb.ingestion.dataobject;

/**
 * <p>
 * Represents DataObject of Table DNB_CIRRUS_BANKRUPTCY_FEED in DynamoDB
 * </p>
 * @author IMPETUS
 * @since 07-May-2015
 * @Project - BigData Initiative <bigdata@impetus.co.in>
 */
public interface BankruptcyFeed
{
  String DUNS_NO = "DUNS_NO";
  String DATE_CREATED = "DATE_CREATED";
  String FILE_NAME = "FILE_NAME";
  String RECORD_NO = "RECORD_NO";
  String BANKRUPTCY_YN = "BANKRUPTCY_YN";

  String HEADER = RECORD_NO + "," + DUNS_NO + "," + DATE_CREATED + "," + FILE_NAME + "," + BANKRUPTCY_YN;
}
