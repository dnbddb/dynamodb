This folder contains the ingest scripts and artefacts for DNB Ingest process

The folder details are described below.
dnb_load_scripts -- Contains the ingestion scripts
    apacheflume  -- Binary distribution for Apache Flume 1.5.2 
    dynamodbfeed -- contains feed/input files for apache flume.
    filechannel  -- contains the checkpoint dir and data dir for file channel used by apache flume.
    flumelogs    -- contains logs for transformation scripts.
    sbin         -- contains the shell scripts for DNB source file ingestion.

Steps to start ingestion process:

1. Extract dnb_load_scripts. To avoid any access issue provide read/write/execute permission to each user.
   chmod -R 777 dnb_load_scripts/

2. Folder small description is given above.

3. Configure Flume - 

   A: Open config file (dnb_load_scripts/apacheflume/conf/dnb_load_scripts/apacheflume/conf)for apache flume. 
      This file is having various properties which get initialized during flume initial start-up. 
      various properties which are configurables are:

	agent.sources : Source aliases from where flume agent will pick up the file. Currently it is set for 10 sources(input files).
		        if you want to add anothere source we can simply add one more variable like spooldirsrc11.

	agent.channels: Channel aliases where flume agent will put the records which are picked up from corresponding agent.sources.
			if you want to add another channel we can simply add one more variable like fileChannel11.

	agent.sinks   : Sink aliases using which flume agent will insert records in dynamoDB.
			if you want to add another sink we can simply add one more variable like dynamodbsink11.

    Here for each aliase having combination of (source/channel/sink) we further defined various properties for each input file/table.
    for example for given table CCS_FSS_FEED here are the properties:
	
	#source config
	agent.sources.spooldirsrc1.type = spooldir
		Type defines type of source. Here since we need spoling on input directory thats why we set value of this variable as 'spooldir'.
	agent.sources.spooldirsrc1.channels = fileChannel1
		Define where flume will put records which are read from source. Here Flume will pick these records from source and put it on filechannel1.
	agent.sources.spooldirsrc1.spoolDir = /home/impadmin/dnb_load_scripts/dynamodbfeed/CCS_FSS_FEED
		Define source directory from where it will pickup the input file.
	
	# Each channel's config.
	agent.channels.fileChannel1.type = file
		Type define type of channel flume will be using. it can be either memory or file.
	agent.channels.fileChannel1.checkpointDir = /home/impadmin/dnb_load_scripts/filechannel/filechannel1/checkpoint/ccsfssfeed
		When flume writes records from source to channel it will create checkpoints at certain interval so that it can recover from failure.
	agent.channels.fileChannel1.dataDirs = /home/impadmin/dnb_load_scripts/filechannel/filechannel1/data/ccsfssfeed
		When flume writes records from source to channel it will create log at this location.logs will be having actual records which are picked from source.

	# Sink config
	agent.sinks.dynamodbsink1.type = com.dnb.dynamodb.ingestion.sink.CCS_FSS_FeedSink
	       Type define type of sink flume will be using. Currently we created a custum sink which is a java class.
	agent.sinks.dynamodbsink1.channel = fileChannel1
	       Define from where flume will pick records. Here Flume will pick these records from filechannel1 to insert them in DB.
	agent.sinks.dynamodbsink1.batch = 20
	      Define batch size. Flume will insert records in DynamoDB in batchs with batchsize defined above.Maximum batch size can be 25. 
	agent.sinks.dynamodbsink1.table = DEV_CCS_FSS_FEED
	      Define table name. Flume will isert records in DynamoDB in given table name.
	agent.sinks.dynamodbsink1.errtable= DEV_CCS_FSS_FEED_ERROR
      	      Define error table name. Flume will insert bad records in DynamoDB in given errortable name.

	If we need to add another input file we we need to provide config value for #Source, Channel, Sink as given above.

4. Configure Shell Script to run Flume.

    A: Go to (/home/impadmin/dnb_load_scripts/sbin/cfg) folder. In this folder there are three config file which shell script 
       required before execute shell script. These are:
     	common.properties	: To configure Log Location 
	DBcredentials.properties: Provide access key and access value to connect to dynamoDB.
	tableCounterConfig.json	: Provide initial value of counter for each table from where we want to generate record numbers.

5. When all above config changes are made, Flume is ready ro run. Go to (dnb_load_scripts/sbin)
   Execute transformation_flume_script.sh. 
	./transformation_flume_script.sh
   Please Note: We always need to run this shell script from sbin folder.


6. Moniter logs at defined log location in case records are not getting inserted in DB.